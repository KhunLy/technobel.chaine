<?php


namespace Core\Config\Types;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Yaml;

class TypesRegistration
{
    /**
     * @var ContainerBuilder
     */
    private $container;

    public function __construct(ContainerBuilder $container){
        $this->container = $container;
    }

    public function __invoke(){
        $postTypes = Yaml::parseFile(TB_PLUGIN_DIR . '/src/Config/post_types.yml');
        $taxonomyTypes = Yaml::parseFile(TB_PLUGIN_DIR . '/src/Config/taxonomy_types.yml');

        foreach($postTypes as $key => $value) {
            $this->simpleRegisterPostType($key, $value['options']);
            $instance = $this->container->get($value['class']);
            $instance->initialize();
        }

        foreach($taxonomyTypes as $key => $value) {
            $pluginTypesNames = array_map(function($post) {
                return TB_PREFIX . $post;
            }, $value['plugin_types'] ?: []);
            $this->simpleRegisterTaxonomyType($key, array_merge($pluginTypesNames, $value['post_types'] ?: []), $value['options']);
            $instance = $this->container->get($value['class']);
            $instance->initialize();
        }
    }

    private function simpleRegisterPostType($typeName, $options = null) {
        if($options === null) {
            $options = [
                'public' => true,
                'menu_position' => 6,
                'supports' => [
                    'title',
                ],
                'show_in_rest' => true,
                'rewrite' => ['slug' => $typeName]
            ];
        }
        register_post_type(TB_PREFIX . $typeName, $options);
    }

    private function simpleRegisterTaxonomyType($taxonomyName, $postTypeNames, $options = null) {
        if($options === null) {
            $options = [
                'public' => true,
                'show_in_rest' => true,
            ];
        }
        register_taxonomy(
            TB_PREFIX . $taxonomyName,
            $postTypeNames,
            $options
        );
    }
}