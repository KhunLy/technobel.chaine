<?php


namespace Core\Config\Routes;


use PHPMailer\PHPMailer\Exception;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Yaml;
use WP_REST_Response;

class RoutesConfiguration
{
    private $container;

    public function __construct(ContainerBuilder $container){
        $this->container = $container;
    }

    public function __invoke()
    {
        $routes = Yaml::parseFile(TB_PLUGIN_DIR . '/src/Config/api_routes.yml');;
        foreach($routes as $route) {
            register_rest_route( 'api', $route['path'], [
                'methods' => $route['method'],
                'callback' => function(\WP_REST_Request $request) use($route){
                    try{
                        $controller = $this->container->get($route['class']);
                        return call_user_func([$controller, $route['action']], $request);
                    }
                    catch (Exception $e) {
                        $response = new WP_REST_Response(['message' => $e->errorMessage()]);
                        $response->set_status(500);
                        return $response;
                    }
                }
            ]);
        }
    }
}