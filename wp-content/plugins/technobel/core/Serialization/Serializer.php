<?php


namespace Core\Serialization;


use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Visitor\Factory\JsonSerializationVisitorFactory;

class Serializer
{
    private $jmsSerializer;

    public function __construct() {
        $builder = SerializerBuilder::create();
        $builder->addDefaultDeserializationVisitors();
        $factory = new JsonSerializationVisitorFactory();
        $factory->setOptions(JSON_UNESCAPED_UNICODE);
        $builder->setSerializationVisitor('json', $factory);
        $this->jmsSerializer =  $builder->build();
    }

    /**
     * @param $oject
     * @return string
     */
    public function serialize($object) {
        return $this->jmsSerializer->serialize($object, 'json');
    }

    /**
     * @param string $data
     * @param string $type
     * @return mixed
     */
    public function deserialize(string $data, string $type, $isArray = false) {
        $data = str_replace('\\', '', $data);
        if($isArray) {
            return $this->jmsSerializer->deserialize($data, 'array<' . $type . '>', 'json');
        }
        return $this->jmsSerializer->deserialize($data, $type, 'json');
    }
}