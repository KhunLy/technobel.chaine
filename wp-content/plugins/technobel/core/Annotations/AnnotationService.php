<?php


namespace Core\Annotations;


use Doctrine\Common\Annotations\AnnotationReader;
use PHPMailer\PHPMailer\Exception;
use ReflectionClass;

class AnnotationService
{

    /**
     * @var AnnotationReader
     */
    private $reader;

    public function __construct(AnnotationReader $reader) {
        $this->reader = $reader;
    }

    public function getPostName($caller) {
        return TB_PREFIX . $this->getClassAnnotation($caller, Post::class, 'name');
    }

    public function getTaxonomyName($caller) {
        return TB_PREFIX . $this->getClassAnnotation($caller, Taxonomy::class, 'name');
    }

    private function getClassAnnotation($caller, $annotationName, $property){
        try {
            $reflectionClass = new ReflectionClass(get_class($caller));
            return $this->reader->getClassAnnotation(
                $reflectionClass,
                $annotationName
            )->$property;
        } catch (Exception $e) {
            throw new Exception(sprintf('Missing annotations %s on %s', $annotationName, get_class($caller)));
        }
    }
}