<?php

namespace Core\Types;

abstract class PostTypeBase extends TypeBase {

    protected $post;

    public function __construct($post) {
        $this->post = $post;
    }

    public function initialize() {
        add_action( 'add_meta_boxes', [$this, 'initExtraFields'] );
        add_action( 'save_post_' . TB_PREFIX . $this->post, [$this, 'saveExtraFields'], 10, 3 );
        $this->init();
    }

    protected function addMetaBox($name, $template, $side = false) {
        if ($side) {
            add_meta_box(
                TB_PREFIX . $this->post . '_' . $name,
                $name,
                function() use($template) {
                    include $template;
                },
                TB_PREFIX . $this->post
            );
        }
        else {
            add_meta_box(
                TB_PREFIX . $this->post . '_' . $name,
                $name,
                function() use($template) {
                    include $template;
                },
                TB_PREFIX . $this->post,
                'side'
            );
        }
    }
}