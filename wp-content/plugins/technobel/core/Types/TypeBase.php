<?php


namespace Core\Types;


abstract class TypeBase
{
    public abstract function init();

    /**
     * @return array
     */
    protected function getCurrentUserRoles() {
        if( is_user_logged_in() ) {
            $user = wp_get_current_user();
            return $roles = (array)$user->roles;
        }
        return [];
    }
}