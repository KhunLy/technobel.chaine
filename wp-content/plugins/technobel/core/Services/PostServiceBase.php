<?php


namespace Core\Services;


use Core\Annotations\AnnotationService;
use Core\Serialization\Serializer;

class PostServiceBase
{
    /**
     * @var string
     */
    protected $post;

    /**
     * @var Serializer
     */
    protected $serializer;

    public function __construct(AnnotationService $annotationService, Serializer $serializer)
    {
        $this->post = $annotationService->getPostName($this);
        $this->serializer = $serializer;
    }
}