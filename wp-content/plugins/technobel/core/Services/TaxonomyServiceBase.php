<?php


namespace Core\Services;


use Core\Annotations\AnnotationService;
use Core\Serialization\Serializer;

abstract class TaxonomyServiceBase
{
    /**
     * @var string
     */
    protected $taxonomy;

    /**
     * @var Serializer
     */
    protected $serializer;

    public function __construct(AnnotationService $annotationService, Serializer $serializer)
    {
        $this->taxonomy = $annotationService->getTaxonomyName($this);
        $this->serializer = $serializer;
    }
}