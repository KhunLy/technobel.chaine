<?php


namespace Core\Module;


use Core\Config\Routes\RoutesConfiguration;
use Core\Config\Types\TypesRegistration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


abstract class ModuleBase
{
    protected $containerBuilder;

    public function __construct() {
        $this->containerBuilder = new ContainerBuilder();
    }

    public function initialize() {
        $this->registerServices();
        add_action('init', new TypesRegistration($this->containerBuilder));
        add_action('rest_api_init', new RoutesConfiguration($this->containerBuilder));
    }

    private function registerServices() {
        $loader = new YamlFileLoader($this->containerBuilder, new FileLocator(TB_PLUGIN_DIR . '/src/Config'));
        $loader->load('services.yml');
    }
}