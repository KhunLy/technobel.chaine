<?php
/*
Plugin Name: Technobel
Author: BStorm
Version: 1.0
*/

use Core\Module\ModuleBase;

define('TB_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('TB_PLUGIN_URI', plugin_dir_url(__FILE__));
define('TB_PREFIX', 'tb_');

define('TB_JS_DATAS', [
    'URI' => TB_PLUGIN_URI,
    'ASSETS_URI' => TB_PLUGIN_URI . 'assets/',
    'DEFAULT_IMG' => TB_PLUGIN_URI . 'assets/images/default.png'
]);

include_once __DIR__ . '/vendor/autoload.php';

register_activation_hook(__FILE__, 'load');
register_deactivation_hook(__FILE__, 'unload');

/* assets */
add_action('admin_enqueue_scripts', function() {
    // register styleSheets
    wp_enqueue_style('tb_admin_css', TB_PLUGIN_URI . 'assets/css/admin.css');
    // register scripts
    wp_enqueue_script('axios', TB_PLUGIN_URI . 'node_modules/axios/dist/axios.min.js');
    wp_enqueue_script('guid', TB_PLUGIN_URI . 'node_modules/guid/guid.js');
    wp_enqueue_script('tb_rendering', TB_PLUGIN_URI . 'assets/js/lib/rendering.js');
    wp_enqueue_script('tb_file_dialog', TB_PLUGIN_URI . 'assets/js/lib/file_dialog.js');
    wp_enqueue_script('tb_observable', TB_PLUGIN_URI . 'assets/js/lib/observable.js', ['guid']);
});

/** remove tag */
function tb_unregister_taxonomy(){
    register_taxonomy('post_tag', array());
}
add_action('init', 'tb_unregister_taxonomy');

// Remove menu
function remove_menus(){
    remove_menu_page('edit-tags.php?taxonomy=post_tag'); // Post tags
}
add_action( 'admin_menu', 'remove_menus' );
/** end remove tag */


add_filter('use_block_editor_for_post', '__return_false', 10);

//add_filter( 'wp_insert_post_data' , 'tb_filter_post_data' , '99', 2 );

//function tb_filter_post_data( $data , $postarr ) {
//    if(is_user_logged_in()) {
//        if(in_array('trainee', wp_get_current_user()->roles))
//            $data['post_status'] = 'pending';
//    }
//    return $data;
//}

function tb_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Projets';
    $submenu['edit.php'][5][0] = 'Projets';
    $submenu['edit.php'][10][0] = 'Ajouter un projet';
    echo '';
}

function tb_change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Projets';
    $labels->singular_name = 'Projet';
    $labels->add_new = 'Ajouter un Projet';
    $labels->add_new_item = 'Ajouter un Projet';
    $labels->edit_item = 'Modifier un Projet';
    $labels->new_item = 'Projet';
    $labels->view_item = 'Voir le Projet';
    $labels->search_items = 'Chercher un Projet';
    $labels->not_found = 'Aucun Projet trouvé';
    $labels->not_found_in_trash = 'Aucun Projet trouvé dans la corbeille';
}
add_action( 'init', 'tb_change_post_object_label' );
add_action( 'admin_menu', 'tb_change_post_menu_label' );

function load() {
    remove_role('subscriber');
    remove_role('author');
    remove_role('contributor');
    remove_role('editor');
    add_role('trainee', 'Stagiaire', [

    ]);
    add_role('staff', 'Staff', [

    ]);

    include_once __DIR__ . '/../../../wp-admin/includes/upgrade.php';


    function tb_init_tables() {
        global $wpdb;
        $tableName = $wpdb->prefix . "socials_clicks";
        maybe_create_table($tableName, "CREATE TABLE " . $tableName ." (
            id INT PRIMARY KEY AUTO_INCREMENT,
            `type` VARCHAR(50) NOT NULL,
            `date` DATETIME NOT NULL,
            `trainee` INT NOT NULL,
            `training` INT NOT NULL
        )");

    }

    tb_init_tables();
}

function unload() {
//    remove_role('trainee');
//    remove_role('staff');
}

/** remove comments */
add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;

    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});
/** end remove comments */

function tb_add_edit_tag_columns($columns){
    $columns['color'] = 'Color';
    unset($columns['slug']);
    unset($columns['description']);
    return $columns;
}
add_filter('manage_edit-category_columns', 'tb_add_edit_tag_columns');
add_filter('manage_edit-tb_skills_columns', 'tb_add_edit_tag_columns');
add_filter('manage_edit-tb_soft_skills_columns', 'tb_add_edit_tag_columns');

function tb_manage_custom_fields($deprecated, $column_name, $term_id)
{
    if ($column_name == 'color') {
        $term = get_term($term_id);
        $color = get_field('color', $term);
        echo '<input disabled type="color" value="' . $color . '">';
    }
}
add_filter ('manage_category_custom_column', 'tb_manage_custom_fields', 10,3);
add_filter ('manage_tb_skills_custom_column', 'tb_manage_custom_fields', 10,3);
add_filter ('manage_tb_soft_skills_custom_column', 'tb_manage_custom_fields', 10,3);


add_filter( 'wp_insert_post_data' , 'tb_filter_post_data' , '99', 2 );

function tb_filter_post_data( $data, $postarr) {
    if($data['post_status'] === 'publish' && in_array('trainee', wp_get_current_user()->roles) && $data['post_type'] === 'post') {
        $data['post_status'] = 'pending';
    }
    return $data;
}

add_action('pre_get_posts','tb_restrict_media_library');

function tb_restrict_media_library( $wp_query_obj ) {
    global $current_user, $pagenow;
    if( !is_a( $current_user, 'WP_User') )
        return;
    if( 'admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments' )
        return;
    if( !current_user_can('manage_media_library') )
        $wp_query_obj->set('author', $current_user->ID );
    return;
}


add_action('admin_menu', 'tb_remove_tools', 99);
function tb_remove_tools()
{
    remove_menu_page('tools.php');
}

function bidirectional_acf_update_value( $value, $post_id, $field  ) {

    // vars
    $field_name = $field['name'];
    $field_key = $field['key'];
    $global_name = 'is_updating_' . $field_name;


    // bail early if this filter was triggered from the update_field() function called within the loop below
    // - this prevents an inifinte loop
    if( !empty($GLOBALS[ $global_name ]) ) return $value;


    // set global variable to avoid inifite loop
    // - could also remove_filter() then add_filter() again, but this is simpler
    $GLOBALS[ $global_name ] = 1;


    // loop over selected posts and add this $post_id
    if( is_array($value) ) {

        foreach( $value as $post_id2 ) {

            // load existing related posts
            $value2 = get_field($field_name, $post_id2, false);


            // allow for selected posts to not contain a value
            if( empty($value2) ) {

                $value2 = array();

            }


            // bail early if the current $post_id is already found in selected post's $value2
            if( in_array($post_id, $value2) ) continue;


            // append the current $post_id to the selected post's 'related_posts' value
            $value2[] = $post_id;


            // update the selected post's value (use field's key for performance)
            update_field($field_key, $value2, $post_id2);

        }

    }


    // find posts which have been removed
    $old_value = get_field($field_name, $post_id, false);

    if( is_array($old_value) ) {

        foreach( $old_value as $post_id2 ) {

            // bail early if this value has not been removed
            if( is_array($value) && in_array($post_id2, $value) ) continue;


            // load existing related posts
            $value2 = get_field($field_name, $post_id2, false);


            // bail early if no value
            if( empty($value2) ) continue;


            // find the position of $post_id within $value2 so we can remove it
            $pos = array_search($post_id, $value2);


            // remove
            unset( $value2[ $pos] );


            // update the un-selected post's value (use field's key for performance)
            update_field($field_key, $value2, $post_id2);

        }

    }


    // reset global varibale to allow this filter to function as per normal
    $GLOBALS[ $global_name ] = 0;


    // return
    return $value;

}

add_filter('acf/update_value/name=projects', 'bidirectional_acf_update_value', 10, 3);


class Technobel extends ModuleBase {

}

function tb() {
    global $tb;

    // Instantiate only once.
    if( !isset($tb) ) {
        $tb = new Technobel();
        $tb->initialize();
    }
    return $tb;
}

// Instantiate.
tb();


