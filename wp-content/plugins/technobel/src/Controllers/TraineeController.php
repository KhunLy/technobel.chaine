<?php


namespace TB\Controllers;

use TB\Models\Trainee;
use TB\Services\StatsService;
use TB\Services\TraineePostService;
use WP_REST_Response;

class TraineeController
{
    private $repo;

    private $statsService;

    public function __construct(TraineePostService $repo, StatsService $service)
    {
        $this->repo = $repo;
        $this->statsService = $service;
    }

    public function post(\WP_REST_Request $request) {
        $user = wp_get_current_user();
        $data = new Trainee($request->get_body_params());
        $post = get_post($data->getId());

        if($post->post_author != $user->ID && !(in_array('staff', $user->roles) || in_array('administrator', $user->roles))) {
            $response = new WP_REST_Response(['message' => 'Vous n\'êtes pas autorisé à modifier cette ressource']);
            $response->set_status(401);
            return $response;
        }

        return $this->repo->update($data);
    }

    public function postMessage(\WP_REST_Request $request)
    {
        $post = $request->get_body_params();
        $to = $post['to'];
        $subject = $post['subject'];
        $email = $post['email'];
        $content = $post['content'];

        if(!isset($to) || !isset($subject) || !isset($email) || !isset($content) || !is_email($email) || !is_email($to)) {
            $response = new WP_REST_Response('Invalid body parameters');
            $response->set_status(400);
            return $response;
        }

        $body = '<p>' . $email . ' vous a envoyé un message</p>';
        $body.= '<p>Contenu: '. $content .'</p>';
        $headers = array('Content-Type: text/html; charset=UTF-8', 'Reply-To: <' . $email . '>');
        wp_mail( $to, $subject, $body, $headers );
    }
}