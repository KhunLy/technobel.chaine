<?php


namespace TB\Controllers;


use TB\Services\StatsService;

class StatsController
{
    private $statsService;

    public function __construct(StatsService $service)
    {
        $this->statsService = $service;
    }

    public function postClick(\WP_REST_Request $request)
    {
        $type = $request->get_body_params()['type'];
        $traineeId = $request->get_body_params()['traineeId'];
        $trainingId = $request->get_body_params()['trainingId'];
        $this->statsService->addClick($type, $traineeId, $trainingId);
    }

    public function getClick(\WP_REST_Request $request)
    {
        return $this->statsService->getAllClicks();
    }
}