<?php


namespace TB\Controllers;


use TB\Services\TrainingPostService;

class TrainingController
{
    private $service;

    public function __construct(TrainingPostService $service)
    {
        $this->service = $service;
    }

    public function get(\WP_REST_Request $request)
    {
        return  $this->service->getTrainings(
            $_GET['offset'],
            $_GET['limit']
        );
    }
}