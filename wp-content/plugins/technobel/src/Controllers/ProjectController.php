<?php


namespace TB\Controllers;


use TB\Services\ProjectPostService;

class ProjectController
{
    private $service;

    public function __construct(ProjectPostService $service)
    {
        $this->service = $service;
    }

    public function get(\WP_REST_Request $request)
    {
        return  $this->service->getProjects(
            $_GET['offset'],
            $_GET['limit'],
            $_GET['sticky']
        );
    }
}