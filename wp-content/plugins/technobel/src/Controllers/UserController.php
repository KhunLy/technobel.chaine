<?php


namespace TB\Controllers;


use Exception;
use TB\Services\TraineePostService;
use WP_REST_Response;

class UserController
{
    private $traineeService;

    public function __construct(TraineePostService $service)
    {
        $this->traineeService = $service;
    }

    public function post(\WP_REST_Request $request) {
        $data = $request->get_body_params();
        try {
            $this->insert($data);
            return true;
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function insert($line) {
        $password = wp_generate_password();
        $id = wp_insert_user([
//            'user_pass' => $password,
            'user_pass' => '1234',
            'user_login' => $line['nom'] . ' ' .$line['prenom'],
            'user_email' => $line['email'],
            'first_name' => $line['prenom'],
            'last_name' => $line['nom'],
            'role' => 'trainee'
        ]);

        $to = $line['email'];
        //$to = 'lykhun@gmail.com';
        $subject = 'Inscription chaine editoriale';
        $body = '<h2>Identifiants:</h2>';
        $body.= '<p>login: <strong>'. $line['nom'] . ' ' .$line['prenom'] .'</strong></p>';
        $body.= '<p>password: <strong>'. $password .'</strong></p>';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        wp_mail( $to, $subject, $body, $headers );

        if(!$id instanceof \WP_Error) {
            global $wpdb;
            $training_name = trim($line['formation']);
            $training_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE TRIM(post_title) LIKE '$training_name' AND post_type LIKE 'tb_trainings' AND post_status NOT LIKE 'trash'");
            if(!$training_id) {
                $training_id = wp_insert_post([
                    'post_title' => $training_name,
                    'post_type' => 'tb_trainings',
                    'post_author' => 1,
                    'post_status' => 'draft'
                ]);
            }
            $this->traineeService->lightInsert($id,$line['email'],$line['nom'] . ' ' . $line['prenom'], $training_id);
        }
        else {
            throw new Exception($id->get_error_message());
        }
    }
}