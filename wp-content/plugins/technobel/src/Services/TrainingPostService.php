<?php


namespace TB\Services;


use Core\Annotations\Post;
use Core\Services\PostServiceBase;
use TB\Models\Training;

/**
 * Class TrainingPostService
 * @package TB\Services
 * @Post(name="trainings")
 */
class TrainingPostService extends PostServiceBase
{
    public function getTrainings($offset = 0, $limit = 4)
    {
        $queryArgs = [
            'post_type' => $this->post,
            'post_status' => 'publish',
            'posts_per_page' => $limit,
            'offset' => $offset,
            'order_by' => 'publish_date',
            'order' => 'desc'
        ];
        $loop = new \WP_Query($queryArgs);
        wp_reset_postdata();
        $results = [];

        while ($loop->have_posts()) {
            $loop->the_post();
            $results[] = Training::toModel();
        }
        return $results;
    }
}