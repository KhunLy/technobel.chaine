<?php


namespace TB\Services;


class StatsService
{
    public function addClick($type, $traineeId, $trainingId)
    {
        global $wpdb;
        $wpdb->insert($wpdb->prefix . 'socials_clicks', [
            'type' => $type,
            'date' => current_time('mysql', 1),
            'trainee' => $traineeId,
            'training' => $trainingId
        ]);
    }

    public function getAllClicks($year = null)
    {
        if($year == null) {
            $year = date("Y");
        }
        global $wpdb;
        return $wpdb->get_results(
            'SELECT COUNT(*) as total, `type`, YEAR(`date`) as year, MONTH(`date`) as month, p.post_title as training FROM ' . $wpdb->prefix . 'socials_clicks c'
            . ' LEFT JOIN ' . $wpdb->prefix . 'posts p ON p.ID = c.training'
            . ' WHERE YEAR(`date`) = '. $year
            .' GROUP BY YEAR(`date`), MONTH(`date`), `type`, `training`');
    }
}