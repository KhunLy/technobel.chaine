<?php


namespace TB\Services;

use Core\Services\PostServiceBase;
use TB\Models\Project;

/**
 * @Core\Annotations\Post(name="projects")
 */
class ProjectPostService extends PostServiceBase
{
    public function getProjects($offset = 0, $limit = 4, $sticky = null)
    {
        $queryArgs = [
            'post_type' => ['post', 'tb_infos'],
            'post_status' => 'publish',
            'posts_per_page' => $limit,
            'offset' => $offset,
            'ignore_sticky_posts' => 1,
            'order_by' => 'publish_date',
            'order' => 'desc'
        ];
        if ($sticky != null) {
            if($sticky) {
                $queryArgs['post__in'] = get_option( 'sticky_posts' );
            }
            else {
                $queryArgs['post__not_in'] = get_option( 'sticky_posts' );
            }
        }
        $loop = new \WP_Query($queryArgs);
        wp_reset_postdata();
        $results = [];

        while ($loop->have_posts()) {
            $loop->the_post();
            $results[] = Project::toModel();
        }
        return $results;
    }
}