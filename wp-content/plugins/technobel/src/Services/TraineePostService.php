<?php


namespace TB\Services;

use Core\Services\PostServiceBase;
use TB\Models\Trainee;
use TB\Models\TraineeExperience;

/**
 * Class TraineePostService
 * @package TB\Services
 * @Core\Annotations\Post(name="trainees")
 */
class TraineePostService extends PostServiceBase
{
    public function insertExperiences($traineeId, array $experiences) {
        add_post_meta($traineeId, 'experiences', $this->serializer->serialize($experiences), true);
    }

    public function updateExperiences($traineeId, array $experiences, $oldValue) {
        $the_post = wp_is_post_revision( $traineeId );
        if ( $the_post ) {
            $traineeId = $the_post;
        }
        update_post_meta($traineeId, 'experiences', $this->serializer->serialize($experiences), $oldValue);
    }

    public function insertStudies($traineeId, array $studies) {
        add_post_meta($traineeId, 'studies', $this->serializer->serialize($studies), true);
    }

    public function updateStudies($traineeId, array $studies, $oldValue) {
        update_post_meta($traineeId, 'studies', $this->serializer->serialize($studies), $oldValue);
    }

    public function insertRealisations($traineeId, array $realisations) {
        add_post_meta($traineeId, 'realisations', $this->serializer->serialize($realisations), true);
    }

    public function updateRealisations($traineeId, array $realisations, $oldValue) {
        update_post_meta($traineeId, 'realisations', $this->serializer->serialize($realisations), $oldValue);
    }

    public function lightInsert($userId, $email, $fullName, $training) {
        $id = wp_insert_post([
            'post_author' => $userId,
            'post_title' => $fullName,
            'post_type' => $this->post
        ]);
        add_post_meta($id, 'email', $email);
        if($training != null) {
            add_post_meta($id, 'training', $training);
        }
    }

    public function update(Trainee $trainee)
    {
        $currentPost = get_post($trainee->getId());

        if($currentPost->post_status === 'publish') {
            // create staging post if there is already a published post

            // need to get the fields witch cannot be submitted by trainee
            //$badges = array_map(function($badge) {return $badge->ID;}, get_field('badges', $trainee->getId()));
            $training = get_field('training', $trainee->getId());

            $trainee->setId(wp_insert_post([
                'post_author' => $currentPost->post_author,
                'post_title' => $this->escapeScript($trainee->getTitle()),
                'post_status' => $this->escapeScript($trainee->getPostStatus()),
                'post_type' => $this->post,
                'post_date' => $currentPost->post_date,
            ]));
            //update_field('badges', $badges, $trainee->getId());
            update_field('training', $training, $trainee->getId());
        }

        else {
            wp_update_post([
                'ID' => $trainee->getId(),
                'post_title' => $this->escapeScript($trainee->getTitle()),
                'post_status' => $this->escapeScript($trainee->getPostStatus())
            ]);
        }

        update_post_meta($trainee->getId(), '_thumbnail_id', $trainee->getThumbnail());
        wp_set_post_terms($trainee->getId(), json_decode($trainee->getSkills()), 'tb_skills');
        wp_set_post_terms($trainee->getId(), json_decode($trainee->getSoftSkills()), 'tb_soft_skills');

        $this->updateAcfFields($trainee);
        return $trainee->getId();
    }

    private function updateAcfFields(Trainee $trainee) {

        $traineeId = $trainee->getId();

        update_field('question_training', $this->escapeScript($trainee->getQuestionTraining()), $traineeId);
        update_field('question_job_1', $this->escapeScript($trainee->getQuestionJob1()), $traineeId);
        update_field('question_job_2', $this->escapeScript($trainee->getQuestionJob2()), $traineeId);
        update_field('question_business_1', $this->escapeScript($trainee->getQuestionBusiness1()), $traineeId);
        update_field('question_business_2', $this->escapeScript($trainee->getQuestionBusiness2()), $traineeId);
        update_field('question_more', $this->escapeScript($trainee->getQuestionMore()), $traineeId);

        update_field('professional_project', $this->escapeScript($trainee->getProfessionalProject()), $traineeId);
        update_field('experiences_short_description', $this->escapeScript($trainee->getExperiencesShortDescription()), $traineeId);
        update_field('studies_short_description', $this->escapeScript($trainee->getStudiesShortDescription(), $traineeId));

        update_field('hobbies', $this->escapeScript($trainee->getHobbies()), $traineeId);
        update_field('cv', $trainee->getCv(), $traineeId);
        update_field('cv_en', $trainee->getCvEn(), $traineeId);
        update_field('projects', json_decode($trainee->getProjects()), $traineeId);

        update_field('email', $this->escapeScript($trainee->getEmail()), $traineeId);
        update_field('phone', $this->escapeScript($trainee->getPhone()), $traineeId);
        update_field('facebook', $this->escapeScript($trainee->getFacebook()), $traineeId);
        update_field('instagram', $this->escapeScript($trainee->getInstagram()), $traineeId);
        update_field('forem', $this->escapeScript($trainee->getForem()), $traineeId);
        update_field('linkedin', $this->escapeScript($trainee->getLinkedin()), $traineeId);

    }

    private function escapeScript($html) {
        if($html == null) return null;
        return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
    }
}