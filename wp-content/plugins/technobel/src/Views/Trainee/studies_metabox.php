<?php
global $post;
$studies = get_post_meta($post->ID, 'studies', true) ? get_post_meta($post->ID, 'studies', true) : '[]';
wp_enqueue_script('admin_studies', TB_PLUGIN_URI . 'assets/js/admin/studies.js', [
    'axios', 'tb_rendering', 'tb_observable', 'tb_file_dialog'
], null, true);
wp_localize_script('admin_studies', 'TB_PLUGIN_STUDIES', [ 'model' => $studies]);
wp_localize_script('admin_studies', 'TB_PLUGIN_CONST', TB_JS_DATAS);
?>
<div>
    <div class="form">
        <div class="d-flex">
            <div class="form-group group-img">
                <div class="img-container clickable">
                    <img id="current-study-img" src="<?= TB_PLUGIN_URI . '/assets/images/default.png'?>">
                </div>
            </div>
            <div class="full" style="padding-left: 10px">
                <div class="form-group">
                    <input placeholder="intitulé" type="text" id="study-title"/>
                </div>
                <div class="form-group">
                    <textarea type="text" id="study-description"></textarea>
                </div>
                <input placeholder="description" type="hidden" name="studies" id="studies-input">
            </div>
        </div>
        <button disabled type="button" id="add-study-button" class="button">Ajouter</button>
    </div>

    <ul id="studies" class="admin-list">

    </ul>
</div>