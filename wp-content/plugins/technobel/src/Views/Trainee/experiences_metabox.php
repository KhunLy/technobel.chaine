<?php
global $post;
$experiences = get_post_meta($post->ID, 'experiences', true) ? get_post_meta($post->ID, 'experiences', true)  : '[]';
wp_enqueue_script('admin_experiences', TB_PLUGIN_URI . 'assets/js/admin/experiences.js', [
    'axios', 'tb_rendering', 'tb_observable', 'tb_file_dialog'
], null, true);
wp_localize_script('admin_experiences', 'TB_PLUGIN_EXPERIENCES', [ 'model' => $experiences]);
wp_localize_script('admin_experiences', 'TB_PLUGIN_CONST', TB_JS_DATAS);
?>
<div>
    <div class="form">
        <div class="d-flex">
            <div class="form-group group-img">
                <div class="img-container clickable">
                    <img id="current-experience-img" src="<?= TB_PLUGIN_URI . '/assets/images/default.png'?>">
                </div>
            </div>
            <div class="full" style="padding-left: 10px">
                <div class="form-group">
                    <input placeholder="intitulé" type="text" id="experience-title"/>
                </div>
                <div class="form-group">
                    <textarea placeholder="description" type="text" id="experience-description"></textarea>
                </div>
                <input type="hidden" name="experiences" id="experiences-input">
            </div>

        </div>
        <button disabled type="button" id="add-experience-button" class="button">Ajouter</button>
    </div>

    <ul id="experiences" class="admin-list">

    </ul>
</div>
