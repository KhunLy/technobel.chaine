<?php


namespace TB\Models;

use JMS\Serializer\Annotation\Type;


class TraineeExperience
{
    /**
     * @var string
     * @Type("string")
     */
    private $title;

    /**
     * @var int
     * @Type("int")
     */
    private $order;

    /**
     * @var string
     * @Type("string")
     */
    private $description;

    /**
     * @var string
     * @Type("string")
     */
    private $image;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return TraineeExperience
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return TraineeExperience
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return TraineeExperience
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return TraineeExperience
     */
    public function setImageId($image)
    {
        $this->image = $image;
        return $this;
    }


}