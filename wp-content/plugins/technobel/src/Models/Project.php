<?php


namespace TB\Models;

use JMS\Serializer\Annotation\Type;

class Project implements \JsonSerializable
{
    /**
     * @var int
     * @Type("int")
     */
    private $id;

    /**
     * @var string
     * @Type("string")
     */
    private $title;

    /**
     * @var string
     * @Type("string")
     */
    private $content;

    /**
     * @var string
     * @Type("string")
     */
    private $image;

    /**
     * @var string
     * @Type("string")
     */
    private $startDate;

    /**
     * @var string
     * @Type("string")
     */
    private $endDate;

    /**
     * @var Taxonomy[]
     * @Type("array<TB\Models\Taxonomy>")
     */
    private $categories;

    /**
     * @var Taxonomy[]
     * @Type("array<TB\Models\Taxonomy>")
     */
    private $skills;

    /**
     * @var string
     * @Type("string")
     */
    private $link;

    /**
     * @var string
     * @Type("string")
     */
    private $excerpt;

    /**
     * @var string
     * @Type("string")
     */
    private $type;

    public function __construct()
    {
        $this->categories = [];
        $this->skills = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Project
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Project
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Project
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return Project
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return Project
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return Taxonomy[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Taxonomy[] $categories
     * @return Project
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param Taxonomy $categorie
     * @return Project
     */
    public function addCategories($categorie)
    {
        $this->categories[] = $categorie;
        return $this;
    }

    /**
     * @return Taxonomy[]
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param Taxonomy[] $skills
     * @return Project
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @param Taxonomy $skill
     * @return Project
     */
    public function addSkill($skill)
    {
        $this->skills[] = $skill;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Project
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @param string $excerpt
     * @return Project
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Project
     */
    public function setType(string $type): Project
    {
        $this->type = $type;
        return $this;
    }



    public static function toModel() {
        $result = new Project();
        $result->setId(get_the_ID());
        $result->setTitle(get_the_title());
        $result->setContent(get_the_content());
        $result->setImage(get_the_post_thumbnail_url());
        $result->setExcerpt(get_the_excerpt());
        $result->setLink(get_the_permalink());
        $result->setStartDate(get_post_meta(get_the_ID(), 'start_date', true));
        $result->setEndDate(get_post_meta(get_the_ID(), 'end_date', true));
        $result->setType(get_post_type(get_the_ID()));
        $terms = get_the_terms(get_the_ID(), TB_PREFIX . 'skills') ?: [];
        foreach ($terms as $term) {
            $result->addSkill(Taxonomy::toModel($term, TB_PREFIX . 'skills'));
        }
        $categories = get_the_category(get_the_ID()) ?: [];
        foreach ($categories as $term) {
            $result->addCategories(Taxonomy::toModel($term, 'category'));
        }
        return $result;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}