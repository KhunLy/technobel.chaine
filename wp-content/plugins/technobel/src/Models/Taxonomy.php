<?php


namespace TB\Models;

use JMS\Serializer\Annotation\Type;

class Taxonomy implements \JsonSerializable
{
    /**
     * @var int
     * @Type("int")
     */
    private $id;

    /**
     * @var string
     * @Type("string")
     */
    private $type;

    /**
     * @var string
     * @Type("string")
     */
    private $value;

    /**
     * @var string
     * @Type("string")
     */
    private $color;

    /**
     * @var string
     * @Type("string")
     */
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Taxonomy
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Taxonomy
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Taxonomy
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Taxonomy
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Taxonomy
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public static function toModel($term, $type)
    {
        $result = new Taxonomy();
        $result->setId($term->term_id);
        $result->setDescription($term->description);
        $result->setValue($term->name);
        $result->setType($type);
        $result->setColor(get_field('color', $term));
        return $result;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}