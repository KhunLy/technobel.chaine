<?php


namespace TB\Models;


use JMS\Serializer\Annotation\Type;

class Training implements \JsonSerializable
{
    /**
     * @var int
     * @Type("int")
     */
    private $id;

    /**
     * @var string
     * @Type("string")
     */
    private $title;

    /**
     * @var string
     * @Type("string")
     */
    private $content;

    /**
     * @var string
     * @Type("string")
     */
    private $image;

    /**
     * @var Taxonomy[]
     * @Type("array<TB\Models\Taxonomy>")
     */
    private $skills;

    /**
     * @var string
     * @Type("string")
     */
    private $link;

    /**
     * @var string
     * @Type("string")
     */
    private $excerpt;

    /**
     * @var string
     * @Type("string")
     */
    private $color;

    public function __construct()
    {
        $this->skills = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Training
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Training
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Training
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Training
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return Taxonomy[]
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param Taxonomy[] $skills
     * @return Training
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @param Taxonomy $skill
     * @return Training
     */
    public function addSkill($skill)
    {
        $this->skills[] = $skill;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Training
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * @param string $excerpt
     * @return Training
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Training
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public static function toModel() {
        $result = new Training();
        $result->setId(get_the_ID());
        $result->setTitle(get_the_title());
        $result->setContent(get_the_content());
        $result->setImage(get_the_post_thumbnail_url());
        $result->setExcerpt(get_the_excerpt());
        $result->setLink(get_the_permalink());
        $result->setColor(get_field('color'));
        $terms = get_the_terms(get_the_ID(), 'tb_skills') ?: [];
        foreach ($terms as $term) {
            $result->addSkill(Taxonomy::toModel($term, 'tb_skills'));
        }
        return $result;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}