<?php


namespace TB\Models;


use JMS\Serializer\Annotation\Type;

class TraineeRealisation
{
    /**
     * @var string
     * @Type("string")
     */
    private $type;

    /**
     * @var string
     * @Type("string")
     */
    private $url;

    /**
     * @var string
     * @Type("string")
     */
    private $name;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return TraineeRealisation
     */
    public function setType(string $type): TraineeRealisation
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return TraineeRealisation
     */
    public function setUrl(string $url): TraineeRealisation
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TraineeRealisation
     */
    public function setName(string $name): TraineeRealisation
    {
        $this->name = $name;
        return $this;
    }

}