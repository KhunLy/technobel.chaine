<?php


namespace TB\Models;

use JMS\Serializer\Annotation\Type;

class Trainee implements \JsonSerializable
{
    public function __construct($post)
    {
        $this->id = $post['id'];
        $this->title = $post['title'];
        $this->cv = $post['cv'];
        $this->cvEn = $post['cvEn'];
        $this->thumbnail = $post['thumbnail'];
        $this->questionTraining = $post['questionTraining'];
        $this->questionJob1 = $post['questionJob1'];
        $this->questionJob2 = $post['questionJob2'];
        $this->questionBusiness1 = $post['questionBusiness1'];
        $this->questionBusiness2 = $post['questionBusiness2'];
        $this->questionMore = $post['questionMore'];
        $this->skills = $post['skills'];
        $this->softSkills = $post['softSkills'];
        $this->professionalProject = $post['professionalProject'];
        $this->realisations = $post['realisations'];
        $this->experiencesShortDescription = $post['experiencesShortDescription'];
        $this->experiences = $post['experiences'];
        $this->studiesShortDescription = $post['studiesShortDescription'];
        $this->studies = $post['studies'];
        $this->hobbies = $post['hobbies'];
        $this->phone = $post['phone'];
        $this->linkedin = $post['linkedin'];
        $this->instagram = $post['instagram'];
        $this->facebook = $post['facebook'];
        $this->forem = $post['forem'];
        $this->email = $post['email'];
        $this->projects = $post['projects'];
        $this->postStatus =$post['postStatus'];
    }

    private $id;

    private $title;

    private $cv;
    private $cvEn;

    private $thumbnail;

    private $questionTraining;
    private $questionJob1;
    private $questionJob2;
    private $questionBusiness1;
    private $questionBusiness2;
    private $questionMore;

    private $skills;

    private $softSkills;

    private $professionalProject;

    private $realisations;

    private $experiencesShortDescription;

    private $experiences;

    private $studiesShortDescription;

    private $studies;

    private $hobbies;

    private $phone;

    private $linkedin;

    private $instagram;

    private $facebook;
    private $forem;

    private $email;

    private $projects;

    private $postStatus;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Trainee
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Trainee
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param mixed $cv
     * @return Trainee
     */
    public function setCv($cv)
    {
        $this->cv = $cv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCvEn()
    {
        return $this->cvEn;
    }

    /**
     * @param mixed $cvEn
     * @return Trainee
     */
    public function setCvEn($cvEn)
    {
        $this->cvEn = $cvEn;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     * @return Trainee
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestionTraining()
    {
        return $this->questionTraining;
    }

    /**
     * @param mixed $questionTraining
     * @return Trainee
     */
    public function setQuestionTraining($questionTraining)
    {
        $this->questionTraining = $questionTraining;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestionJob1()
    {
        return $this->questionJob1;
    }

    /**
     * @param mixed $questionJob1
     * @return Trainee
     */
    public function setQuestionJob1($questionJob1)
    {
        $this->questionJob1 = $questionJob1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestionJob2()
    {
        return $this->questionJob2;
    }

    /**
     * @param mixed $questionJob2
     * @return Trainee
     */
    public function setQuestionJob2($questionJob2)
    {
        $this->questionJob2 = $questionJob2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestionBusiness1()
    {
        return $this->questionBusiness1;
    }

    /**
     * @param mixed $questionBusiness1
     * @return Trainee
     */
    public function setQuestionBusiness1($questionBusiness1)
    {
        $this->questionBusiness1 = $questionBusiness1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuestionMore()
    {
        return $this->questionMore;
    }

    /**
     * @param mixed $questionMore
     * @return Trainee
     */
    public function setQuestionMore($questionMore)
    {
        $this->questionMore = $questionMore;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getQuestionBusiness2()
    {
        return $this->questionBusiness2;
    }

    /**
     * @param mixed $questionBusiness2
     * @return Trainee
     */
    public function setQuestionBusiness2($questionBusiness2)
    {
        $this->questionBusiness2 = $questionBusiness2;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     * @return Trainee
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSoftSkills()
    {
        return $this->softSkills;
    }

    /**
     * @param mixed $softSkills
     * @return Trainee
     */
    public function setSoftSkills($softSkills)
    {
        $this->softSkills = $softSkills;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfessionalProject()
    {
        return $this->professionalProject;
    }

    /**
     * @param mixed $professionalProject
     * @return Trainee
     */
    public function setProfessionalProject($professionalProject)
    {
        $this->professionalProject = $professionalProject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRealisations()
    {
        return $this->realisations;
    }

    /**
     * @param mixed $realisations
     * @return Trainee
     */
    public function setRealisations($realisations)
    {
        $this->realisations = $realisations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperiencesShortDescription()
    {
        return $this->experiencesShortDescription;
    }

    /**
     * @param mixed $experiencesShortDescription
     * @return Trainee
     */
    public function setExperiencesShortDescription($experiencesShortDescription)
    {
        $this->experiencesShortDescription = $experiencesShortDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperiences()
    {
        return $this->experiences;
    }

    /**
     * @param mixed $experiences
     * @return Trainee
     */
    public function setExperiences($experiences)
    {
        $this->experiences = $experiences;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudiesShortDescription()
    {
        return $this->studiesShortDescription;
    }

    /**
     * @param mixed $studiesShortDescription
     * @return Trainee
     */
    public function setStudiesShortDescription($studiesShortDescription)
    {
        $this->studiesShortDescription = $studiesShortDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStudies()
    {
        return $this->studies;
    }

    /**
     * @param mixed $studies
     * @return Trainee
     */
    public function setStudies($studies)
    {
        $this->studies = $studies;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHobbies()
    {
        return $this->hobbies;
    }

    /**
     * @param mixed $hobbies
     * @return Trainee
     */
    public function setHobbies($hobbies)
    {
        $this->hobbies = $hobbies;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Trainee
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     * @return Trainee
     */
    public function setLinkedIn($linkedin)
    {
        $this->linkedin = $linkedin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param mixed $instagram
     * @return Trainee
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     * @return Trainee
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getForem()
    {
        return $this->forem;
    }

    /**
     * @param mixed $forem
     * @return Trainee
     */
    public function setForem($forem)
    {
        $this->forem = $forem;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Trainee
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param mixed $projects
     * @return Trainee
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostStatus()
    {
        return $this->postStatus;
    }

    /**
     * @param mixed $postStatus
     * @return Trainee
     */
    public function setPostStatus($postStatus)
    {
        $this->postStatus = $postStatus;
        return $this;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}