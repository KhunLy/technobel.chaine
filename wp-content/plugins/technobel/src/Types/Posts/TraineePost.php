<?php

namespace TB\Types\Posts;

use Core\Serialization\Serializer;
use Core\Types\PostTypeBase;
use TB\Models\Trainee;
use TB\Models\TraineeExperience;
use TB\Models\TraineeRealisation;
use TB\Models\TraineeStudy;
use TB\Services\TraineePostService;

class TraineePost extends PostTypeBase {

    /**
     * @var TraineePostService
     */
    private $service;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(TraineePostService $service, Serializer $serializer) {
        parent::__construct('trainees');
        $this->service = $service;
        $this->serializer = $serializer;
    }

    public function initExtraFields()
    {
        $this->addMetaBox('Experiences', TB_PLUGIN_DIR . '/src/Views/Trainee/experiences_metabox.php');
        $this->addMetaBox('Etudes', TB_PLUGIN_DIR . '/src/Views/Trainee/studies_metabox.php');
    }

    public function init()
    {
        add_action( 'user_register', [$this,'newUser']);
    }

    public function newUser($userId) {
        if($_POST['role'] === 'trainee') {
            $training = null;
            $trainings = $_POST['acf'];
            if(count($trainings) > 0) {
                $training = array_values($trainings)[0];
            }
            $this->service->lightInsert($userId, $_POST['email'], $_POST['last_name'] . ' ' . $_POST['first_name'], $training);
        }
    }

    public function saveExtraFields($id, $post, $isUpading)
    {
        if(!is_user_logged_in() && wp_get_current_user()->ID != $post->post_author && !in_array('staff', $this->getCurrentUserRoles()) && !in_array('administrator', $this->getCurrentUserRoles())) {
            return;
        }

//        update_field('training', get_field('email', $post->ID), 'user_' . $post->post_author);

        if(isset($_POST['experiences']) && $_POST['experiences']) {
            $experiences = $this->serializer->deserialize($_POST['experiences'], TraineeExperience::class, true);
            if(!is_array($experiences)) {
                return;
            }
            if($isUpading) {
                $this->service->updateExperiences($id, $experiences, get_post_meta($post->ID,'experiences' , true));
            }
            else{
                $this->service->insertExperiences($id, $experiences);
            }
        }
        if(isset($_POST['studies']) && $_POST['studies']) {
            $studies = $this->serializer->deserialize($_POST['studies'], TraineeStudy::class, true);
            if(!is_array($studies)) {
                return;
            }
            if($isUpading) {
                $this->service->updateStudies($id, $studies, get_post_meta($post->ID,'studies' , true));
            }
            else{
                $this->service->insertStudies($id, $studies);
            }
        }
        if(isset($_POST['realisations']) && $_POST['realisations']) {
            $realisations = $this->serializer->deserialize($_POST['realisations'] ?: '[]', TraineeRealisation::class, true);
            if(!is_array($realisations)) {
                return;
            }
            if($isUpading) {
                $this->service->updateRealisations($id, $realisations, get_post_meta($post->ID,'realisations' , true));
            }
            else{
                $this->service->insertRealisations($id, $realisations);
            }
        }

        // is there is 2 publish trainees for the same user delete the older
        if(in_array('staff', $this->getCurrentUserRoles()) || in_array('administrator', $this->getCurrentUserRoles())){
            if ($post->post_status === 'publish') {
                $args = [
                    'post_type' => 'tb_trainees',
                    'post_status' => ['publish'],
                    'posts_per_page' => 2,
                    'offset' => 0,
                    'author__in' => [$post->post_author],
                    'order_by' => 'publish_date',
                    'order' => 'desc'
                ];
                $loop = new \WP_Query($args);
                $loop->the_post();
                if ($loop->have_posts()) {
                    $loop->the_post();
                    wp_delete_post(get_the_ID());
                }
            }
        }
    }
}