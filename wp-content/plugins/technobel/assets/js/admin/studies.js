'use strict';

jQuery($ => {
    /** elements **/
    const $studiesElement = $('#studies');
    const $addStudyButtonElement = $('#add-study-button');
    const $studyTitleInputElement = $('#study-title');
    const studyDescInputElement = $('#study-description');
    const $currentStudyImageElement = $('#current-study-img');
    const $studiesInputElement = $('#studies-input');

    /** observables **/
    const studies$ = new ObservableCollection();
    const currentStudyImage$ = new Observable(null);

    /** templates **/
    const studyTemplate = `
        <li class="study-row">
            <div>
                <div class="img-container">
                    <img src="{{ image }}">
                </div>
                <div class="info">
                    <h4>{{ title }}</h4 >
                    <p>{{ description }}</p>
                </div>
            </div>
            <span class="text-danger remove-study-button">Supprimer</span>
        </li>
    `;

    /** events **/
    $addStudyButtonElement.on('click', addStudy);
    $studiesElement.on('click', '.remove-study-button', removeStudy);
    $currentStudyImageElement.on('click', addStudyImage);
    $studyTitleInputElement.add(studyDescInputElement).on('input', checkStudyValidity);


    /** subscribtions **/
    studies$.subscribe(onStudies);
    currentStudyImage$.subscribe(image => $currentStudyImageElement.attr('src', image || TB_PLUGIN_CONST.DEFAULT_IMG))

    studies$.set(JSON.parse(TB_PLUGIN_STUDIES.model));

    function addStudy(event) {
        event.preventDefault();
        const study = {
            title: $studyTitleInputElement.val(),
            description: studyDescInputElement.val().replace('\n', '<br>'),
            image: currentStudyImage$.value || TB_PLUGIN_CONST.DEFAULT_IMG
        };
        if(!study.title || !study.description) {
            return;
        }
        studies$.add(study);
        $studyTitleInputElement.val(null);
        studyDescInputElement.val(null);
        currentStudyImage$.next(null);
        checkStudyValidity();
    }

    function removeStudy(event) {
        event.preventDefault();
        const index = $(event.target).parent('li').index();
        studies$.remove(index);
    }

    function checkStudyValidity() {
        const invalid = !$studyTitleInputElement.val()?.trim() || !studyDescInputElement.val()?.trim();
        $addStudyButtonElement.prop("disabled", invalid);
    }

    function onStudies(data) {
        const render = $(renderCollection(data, studyTemplate));
        $studiesElement.html(render);
        const values = JSON.stringify(data.map((item,i) => {
            item.order = i;
            return item;
        }));
        $studiesInputElement.val(values);
    }

    async function addStudyImage($event) {
        let urls = await openFileDialog();
        if(urls.length) {
            currentStudyImage$.next(urls[0]);
        }
    }

});