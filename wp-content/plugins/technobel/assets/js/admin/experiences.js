'use strict';

jQuery($ => {
    /** elements **/
    const $experiencesElement = $('#experiences');
    const $addExperienceButtonElement = $('#add-experience-button');
    const $experienceTitleInputElement = $('#experience-title');
    const $experienceDescInputElement = $('#experience-description');
    const $currentExperienceImageElement = $('#current-experience-img');
    const $experiencesInputElement = $('#experiences-input');

    /** observables **/
    const experiences$ = new ObservableCollection();
    const currentExperienceImage$ = new Observable(null);

    /** templates **/
    const experienceTemplate = `
        <li class="exp-row">
            <div>
                <div class="img-container">
                    <img src="{{ image }}">
                </div>
                <div class="info">
                    <h4>{{ title }}</h4 >
                    <p>{{ description }}</p>
                </div>
            </div>
            <span class="text-danger remove-experience-button">Supprimer</span>
        </li>
    `;

    /** events **/
    $addExperienceButtonElement.on('click', addExperience);
    $experiencesElement.on('click', '.remove-experience-button', removeExperience);
    $currentExperienceImageElement.on('click', addExperienceImage);
    $experienceTitleInputElement.add($experienceDescInputElement).on('input', checkExperienceValidity);

    /** subscriptions **/
    experiences$.subscribe(onExperiences);
    currentExperienceImage$.subscribe(image => $currentExperienceImageElement.attr('src', image || TB_PLUGIN_CONST.DEFAULT_IMG))

    experiences$.set(JSON.parse(TB_PLUGIN_EXPERIENCES.model));

    function addExperience(event) {
        event.preventDefault();
        const experience = {
            title: $experienceTitleInputElement.val(),
            description: $experienceDescInputElement.val().replace('\n', '<br>'),
            image: currentExperienceImage$.value || TB_PLUGIN_CONST.DEFAULT_IMG
        };
        if(!experience.title || !experience.description) {
            return;
        }
        experiences$.add(experience);
        $experienceTitleInputElement.val(null);
        $experienceDescInputElement.val(null);
        currentExperienceImage$.next(null);
        checkExperienceValidity();
    }

    function removeExperience(event) {
        event.preventDefault();
        const index = $(event.target).parent('li').index();
        experiences$.remove(index);
    }


    function checkExperienceValidity() {
        const invalid = !$experienceTitleInputElement.val()?.trim() || !$experienceDescInputElement.val()?.trim();
        $addExperienceButtonElement.prop("disabled", invalid);
    }

    function onExperiences(data) {
        const render = $(renderCollection(data, experienceTemplate));
        $experiencesElement.html(render);
        const values = JSON.stringify(data.map((item,i) => {
            item.order = i;
            return item;
        }));
        $experiencesInputElement.val(values);
    }

    async function addExperienceImage($event) {
        let urls = await openFileDialog();

        if(urls.length) {
            currentExperienceImage$.next(urls[0]);
        }
    }
});