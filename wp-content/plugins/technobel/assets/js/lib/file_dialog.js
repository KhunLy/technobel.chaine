function openFileDialog(type = 'image', multiple = false) {
    return new Promise((resolve, reject) => {
        const image_frame = wp.media({
            title: 'Select Media',
            multiple,
            library: {
                type,
            },
            frame: 'post',
            state: 'insert',
        });

        image_frame.on('close', () => {
            const selection = image_frame.state().get('selection');
            const gallery = [];
            selection.each(function (attachment) {
                gallery.push(wp.media.attachment(attachment['id']).get('url'));
            });
            resolve(gallery);
        });

        image_frame.open();
    });
}