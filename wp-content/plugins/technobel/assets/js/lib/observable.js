'use strict';

class Observable {
    constructor(value) {
        this.subscribers = {};
        this.value = value;
        this.subscribe = callback => {
            const id = Guid.raw();
            this.subscribers[id] = callback;
            return id;
        };
        this.next = value => {
            this.value = value;
            for(let callback of Object.values(this.subscribers)) {
                callback(this.value);
            }
        };
        this.unsubscribe = id => {
            delete this.subscribers[id];
        };
    }
}

class ObservableCollection {
    constructor() {
        this.subscribers = {};
        this.collection = [];
        this.subscribe = callback => {
            const id = Guid.raw();
            this.subscribers[id] = callback;
            return id;
        };
        this.clear = () => {
            this.collection = [];
            this.notify();
        };
        this.add = item => {
            this.collection.push(item);
            this.notify();
        };
        this.remove = index => {
            this.collection.splice(index, 1);
            this.notify();
        };
        this.set = collection => {
            this.collection = collection;
            this.notify();
        };
        this.notify = () => {
            for(let callback of Object.values(this.subscribers)) {
                callback(this.collection);
            }
        };
        this.unsubscribe = id => {
            delete this.subscribers[id];
        };
    }
}