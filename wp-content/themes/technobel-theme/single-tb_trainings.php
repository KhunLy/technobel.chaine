<?php get_header() ?>
<?php
$skills = get_the_terms(get_the_ID(), 'tb_skills');
?>
<section>
    <div class="row">
        <div class="col s12">
            <div>
                <div class="training-header" style="background-color: <?php the_field('color') ?>; float:left;margin-right:20px;width:33%;padding-top:33%">
                    <div class="training-header-inner">
                        <h5><?php the_title() ?></h5>
                    </div>
                </div>
                <?php the_content(); ?>
            </div>
        </div>
        <div class="col s12">
            <h6>Technologies</h6>
            <div class="tags">
                <?php if($skills): ?>
                    <?php foreach($skills as $skill): ?>
                        <?php $color = get_field('color', $skill) ?>
                        <div class="tag skill" style="border-color: <?= $color ?>; color: <?= $color ?>">
                            <?= $skill->name ?>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p>Pas encore de technologies</p>
                <?php endif; ?>
            </div>
        </div>
        <div class="col s12">
            <h6>Étudiants de la filières</h6>
            <?php $subloop = tb_get_trainees_by_trainings([get_the_ID()])?>
            <?php if(have_posts()):?>
                <div class="trainees">
                    <?php while ($subloop->have_posts()): $subloop->the_post() ?>
                        <?php get_template_part('template-parts/card', 'trainee'); ?>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_template_part('template-parts/search', 'tb_trainings') ?>
<?php get_footer() ?>