<?php
/*
Template Name: Edit Profile
*/
?>

<?php get_header() ?>
<?php
$user = wp_get_current_user();
if(!is_user_logged_in()) {
    global $wp_query;
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 ); exit();
}
$loop = tb_get_trainee_by_user(wp_get_current_user()->ID, false, in_array('trainee', $user->roles));
$skills = get_the_terms(get_the_ID(), 'tb_skills');
if($loop->have_posts()) {
    $loop->the_post();
}

var_dump(get_field('projects'));die;

$model = (object)[
    'id' => get_the_ID(),
    'title' => get_the_title(),
    'studies' => json_decode(get_post_meta(get_the_ID(), 'studies', true) ?: '[]', false),
    'experiences' => json_decode(get_post_meta(get_the_ID(), 'experiences', true) ?: '[]', false),
    'thumbnail' => get_post_thumbnail_id(),
    'cv' => get_field('cv') ? get_field('cv')['id'] : null,
    'cvEn' => get_field('cv_en') ? get_field('cv_en')['id'] : null,
    'questionTraining' => get_field('question_training'),
    'questionJob1' => get_field('question_job_1'),
    'questionJob2' => get_field('question_job_2'),
    'questionBusiness1' => get_field('question_business_1'),
    'questionBusiness2' => get_field('question_business_2'),
    'questionMore' => get_field('question_more'),
    'professionalProject' => get_field('professional_project'),
    'experiencesShortDescription' => get_field('experiences_short_description'),
    'studiesShortDescription' => get_field('studies_short_description'),
    'email' => get_field('email'),
    'phone' => get_field('phone'),
    'facebook' => get_field('facebook'),
    'linkedin' => get_field('linkedin'),
    'instagram' => get_field('instagram'),
    'forem' => get_field('forem'),
    'hobbies' => get_field('hobbies'),
    'softSkills' => array_map(function($item){ return $item->term_id; } ,get_the_terms(get_the_ID(), 'tb_soft_skills') ?: []),
    'skills' => array_map(function($item){ return $item->term_id; } ,get_the_terms(get_the_ID(), 'tb_skills') ?: []),
    'projects' => array_map(function($item) { return $item->ID; }, get_field('projects') ?: []),
    'allSkills' => tb_get_skills() ?: [],
    'allSoftSkills' => tb_get_soft_skills() ?: [],
    'allProjects' => [],
    'realisations' => json_decode(get_post_meta(get_the_ID(), 'realisations', true) ?: '[]', false),
    'thumbnailUrl' => get_the_post_thumbnail_url(),
    'cvUrl' => get_field('cv') ? get_field('cv')['url'] : null,
    'cvFileName' => get_field('cv') ? get_field('cv')['name'] : null,
    'cvEnUrl' => get_field('cv_en') ? get_field('cv_en')['url'] : null,
    'cvEnFileName' => get_field('cv_en') ? get_field('cv_en')['name'] : null,
];

$loop = tb_get_projects();
while ($loop->have_posts()) {
    $loop->the_post();
    $model->allProjects[] = [ 'id' => get_the_ID(), 'post_title' => get_the_title() ];
}
wp_reset_postdata();

wp_localize_script('theme_profile_js', 'THEME_DATA', [ 'model' => $model]);
?>
<?php get_header() ?>

<div id="react-profile"></div>
<?php get_footer() ?>
