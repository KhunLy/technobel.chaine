const Loader = (props) => {
    const { isLoading } = props;
    return <div className={ 'loader' + (isLoading ? '' : ' hidden') }>
        <i className="cog cog-lg fa fa-cog"/>
        <i className="cog cog-counter cog-md fa fa-cog"/>
        <i className="cog cog-sm fa fa-cog"/>
    </div>;
}

const MoreBtn = (props) => {
    const { isLoading, disabled, text, onClick } = props
    return <div className="more">
        <Loader isLoading={ isLoading }/>
        <button className="tb" onClick={onClick} disabled={ isLoading }>
            <span>{text} </span>
            { (isLoading) && <i className='fa fa-cog fa-spin'/> }
        </button>
    </div>
}

const Card = (props) => {
    const { model } = props;

    const markup = (val) => {
        return { __html: val };
    };

    return <div className="col xl3 m6 s12">
        <a href={ model.link } className="blk">
            <div className="tb-card">
                <div className="card-content">
                    <div className="card-top">
                        <img src={ model.image || THEME_CONST.DEFAULT_IMG } alt="Card image cap" />
                    </div>
                    <div className="card-center">
                        <h5 dangerouslySetInnerHTML={ markup(model.title) }/>
                        <p dangerouslySetInnerHTML={ markup(model.excerpt) }/>
                    </div>
                    <div className="card-bottom">
                        <div className="tags">
                            { model.skills.map(skill =>
                                <div className="tag skill" style={{ color: skill.color, borderColor: skill.color }}>
                                    { skill.value }
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
}

const InfoCard = (props) => {
    const { model } = props;

    const markup = (val) => {
        return { __html: val };
    };

    return <div className="col xl3 m6 s12">
        <a href={ model.link } className="blk">
            <div className="tb-card">
                <div className="ribbon ribbon-top-left"><span>News</span></div>
                <div className="card-content">
                    <div className="card-top">
                        <img src={ model.image || THEME_CONST.DEFAULT_IMG } alt="Card image cap" />
                    </div>
                    <div className="card-center">
                        <h5 dangerouslySetInnerHTML={ markup(model.title) }/>
                        <p dangerouslySetInnerHTML={ markup(model.excerpt) }/>
                    </div>
                </div>
            </div>
        </a>
    </div>;
}

const TrainingCard = (props) => {
    const { model } = props;

    const markup = (val) => {
        return { __html: val };
    };

    return <div className="col xl3 m6 s12">
        <a href={ model.link } className="blk">
            <div className="tb-card card-training">
                <div className="card-content">
                    <div className="card-top training-top" style={ {'background-color': model.color} }>
                        {/* <img src={ model.image || THEME_CONST.DEFAULT_IMG } alt="Card image cap" /> */}
                        <h5 dangerouslySetInnerHTML={ markup(model.title) }/>
                    </div>
                    {/* <div className="card-center">
                        <p dangerouslySetInnerHTML={ markup(model.excerpt) }/>
                    </div>
                    <div className="card-bottom right">
                        <button className="tb inverse">Plus d'info</button>
                    </div> */}
                </div>
            </div>
        </a>
    </div>
}

const Projects = (props) => {

    const { sticky } = props;

    const [ isLoading, setIsLoading ] = React.useState(false);
    const [ projects, setProjects ] = React.useState([]);
    const [ disabled, sedDisabled ] = React.useState(false);
    const [ offset, setOffset ] = React.useState(4);

    let limit = 4;

    const loadItems = async () => {
        setIsLoading(true);
        axios.get(`${THEME_CONST.API_URI}api/projects?limit=${limit}&offset=${offset}&sticky=${sticky}`)
            .then(({ data }) => {
                setProjects([...projects, ...data]);
                if(!data.length) {
                    sedDisabled(true);
                }
                setOffset(offset + data.length);
                console.log(data)
            }).catch(error => {

            }).finally(() => {
                setIsLoading(false);
            });
    }

    React.useEffect(() => {}, []);

    const items = <React.Fragment>
        { projects.map(item => item.type === 'post' ? <Card model={ item }/> : <InfoCard model={ item }/> ) }
    </React.Fragment>

    return <React.Fragment>
        <div className="row">{ items }</div>
        <MoreBtn text="+ de projets" onClick={ loadItems } isLoading={ isLoading } disabled={ disabled }/>
    </React.Fragment>
}

const Trainings = (props) => {

    const [ isLoading, setIsLoading ] = React.useState(false);
    const [ projects, setProjects ] = React.useState([]);
    const [ disabled, sedDisabled ] = React.useState(false);
    const [ offset, setOffset ] = React.useState(4);

    let limit = 4;

    const loadItems = async () => {
        setIsLoading(true);
        axios.get(`${THEME_CONST.API_URI}api/trainings?limit=${limit}&offset=${offset}`)
            .then(({ data }) => {
                setProjects([...projects, ...data]);
                if(!data.length) {
                    sedDisabled(true);
                }
                setOffset(offset + data.length);
            }).catch(error => {

        }).finally(() => {
            setIsLoading(false);
        });
    }

    React.useEffect(() => {}, []);

    const items = <React.Fragment>
        { projects.map(item => <TrainingCard model={ item }/>) }
    </React.Fragment>

    return <React.Fragment>
        <div className="row">{ items }</div>
        <MoreBtn text="+ de filières" onClick={ loadItems } isLoading={ isLoading } disabled={ disabled }/>
    </React.Fragment>
}

ReactDOM.render(<Projects sticky={ 0 } />, document.getElementById('react-last-projects'));
ReactDOM.render(<Projects sticky={ 1 } />, document.getElementById('react-sticky-projects'));
ReactDOM.render(<Trainings />, document.getElementById('react-last-trainings'));

