const ListItem = props => {
    const { item, itemChange, isEditing, itemDelete } = props;

    const markup = (val) => {
        return { __html: val };
    };

    const changeImage = () => {
        openFileDialog().then(files => {
            if(files.length) {
                item.image = files[0].url;
                itemChange();
            }
        });
    };

    return <div className="list-item">
        <div className="img">
            <img className={ isEditing && 'image-editable' }
                 src={ item.image || THEME_CONST.DEFAULT_IMG }
                 onClick={ changeImage }/>
        </div>
        <div className="info">
            <h6 contentEditable={ isEditing }
                dangerouslySetInnerHTML={ markup(item.title) }
                onBlur={ e => {
                    item.title = e.target.innerHTML;
                    itemChange();
                } }/>
            <p contentEditable={ isEditing }
               dangerouslySetInnerHTML={ markup(item.description) }
               onBlur={ e => {
                   item.description = e.target.innerHTML
                   itemChange();
               } }/>
            { isEditing && <p style={ { textAlign: 'right' } }>
                <a className="red-text" style={ { cursor: 'pointer' } } onClick={ itemDelete }>Supprimer</a>
            </p> }
        </div>
    </div>
};

const Realisation = props => {
    const { item, itemChange, isEditing, itemDelete } = props;

    const [ type, setType ] = React.useState(item.type);
    const [ url, setUrl ] = React.useState(item.url);
    const [ name, setName ] = React.useState(item.name);

    const ref = React.createRef();

    React.useEffect(() => {
        //const typeSelect = M.FormSelect.init(ref.current);
        item.type = item.type || 'video';
    }, []);

    const changeFile = () => {
        let type = null;

        if(item.type === 'video' || item.type === 'interview') {
            type = ['video/mpg','video/mp4','video/mpeg'];
        }

        openFileDialog(type).then(files => {
            if(files.length) {
                item.url = files[0].url;
                itemChange();
                setUrl(files[0].url);
            }
        });
    };

    return <div className="realisation">
        <div className="input-field">
            <label>Nom</label>
            <input type="text" value={name} onChange={ e => {
                setName(e.target.value);
                item.name = e.target.value;
                itemChange();
            } } readOnly={ !isEditing }/>
        </div>
        <div className="type" style={{ paddingBottom: '10px' }}>
            <select ref={ref} value={ item.type } onChange={ e => {
                setType(e.target.value);
                setUrl(null);
                item.type = e.target.value;
                itemChange();
            } } disabled={ !isEditing } className="browser-default">
                <option value="video">Video</option>
                <option value="interview">Interview</option>
                <option value="document">Document</option>
                <option value="website">Site Web</option>
                <option value="git">Lien git</option>
                <option value="cube">Cube</option>
            </select>
        </div>
        <div className="attachment">
            { type !== null &&
                <React.Fragment>
                    { (type !== 'website' && type !== 'git') &&
                        <React.Fragment>
                            { isEditing && <label><button onClick={ changeFile } className="tb inverse">Parcourir</button></label> }
                            <div className="input-field">
                                <input readOnly={ true } value={ url }/>
                            </div>
                        </React.Fragment>
                    }
                    { (type === 'website' || type === 'git') &&
                        <div className="input-field">
                            <label>Url</label>
                            <input type="url" value={ url } onChange={ e => {
                                setUrl(e.target.value);
                                item.url = e.target.value
                                itemChange();
                            } } readOnly={ !isEditing }/>
                        </div>
                    }
                </React.Fragment>
            }
        </div>
        { isEditing && <p style={ { textAlign: 'right' } }>
            <a className="red-text" style={ { cursor: 'pointer' } } onClick={ itemDelete }>Supprimer</a>
        </p> }
    </div>;
};

const Profile = props => {

    const [ isLoading, setIsLoading ] = React.useState(null);
    const [ isEditing, setIsEditing ] = React.useState(false);
    const [ oldValues, setOldValues] = React.useState(null);
    const [ model, setModel ] = React.useState(props.model);

    React.useEffect(() => {
        // init
        const skillsSelect = jQuery('#skills').select2();
        const softSkillsSelect = jQuery('#softSkills').select2();
        const projectsSelect = jQuery('#projects').select2();

        skillsSelect.on('change', skillsChanged);
        softSkillsSelect.on('change', softSkillsChanged);
        projectsSelect.on('change', projectsChanged);
    }, []);

    const markup = (val) => {
        return { __html: val };
    };

    const editModel = () => {
        setOldValues({
            ...model,
            studies: model.studies.map(x => ({...x})),
            experiences: model.experiences.map(x => ({...x})),
            projects: [...model.projects],
            skills: [...model.skills],
            softSkills: [...model.softSkills],
        });
        setIsEditing(true);
    };

    const saveModel = (status) => {
        setIsLoading(status);
        axios.post(`${THEME_CONST.API_URI}api/trainees`, getFormData(status), {headers: {
                'content-type': 'application/json',
                'X-WP-Nonce': THEME_CONST.NONCE
            }
        })
            .then(({data}) => {
                toastr.success('Sauvergarde réussie');
                setModel(model => ({...model, id: data}))
                setIsEditing(false);
            }).catch(e => {
                toastr.error('Une erreur est survenue');
            }).finally(() => {
                setIsLoading(null);
            });
    };

    const cancelModel = () => {
        jQuery('#skills').val(oldValues.skills).trigger('change');
        jQuery('#softSkills').val(oldValues.softSkills).trigger('change');
        jQuery('#projects').val(oldValues.projects).trigger('change');
        setModel(oldValues);
        setIsEditing(false);
    };

    const changeThumbnail = async e => {
        const files = await openFileDialog();
        if (files.length) {
            setModel(model => ({
                ...model,
                thumbnailUrl: files[0].url,
                thumbnail: files[0].id
            }));
        }
    }

    const changeCV = async e => {
        const files = await openFileDialog('application/pdf');
        if (files.length) {
            setModel(model => ({
                ...model,
                cvUrl: files[0].url,
                cvFileName: files[0].name,
                cv: files[0].id
            }));
        }
    }

    const changeCVEn = async e => {
        const files = await openFileDialog('application/pdf');
        if (files.length) {
            setModel(model => ({
                ...model,
                cvEnUrl: files[0].url,
                cvEnFileName: files[0].name,
                cvEn: files[0].id
            }));
        }
    }

    const skillsChanged = e => {
        const selection = jQuery(e.target).select2('data').map(item => item.id);
        setModel(model => ({ ...model, skills : selection }));
    }

    const softSkillsChanged = e => {
        const selection = jQuery(e.target).select2('data').map(item => item.id);
        setModel(model => ({ ...model, softSkills : selection  }));
    }

    const projectsChanged = e => {
        const selection = jQuery(e.target).select2('data').map(item => item.id);
        setModel(model => ({ ...model, projects : selection  }));
    }

    const getFormData = (status) => {
        const fd = new FormData();
        fd.append('postStatus', status);
        for(const key of Object.keys(model).filter(k => !(['allSkills', 'allSoftSkills', 'allProjects'].includes(k)) && model[k] != null)) {
            if(Array.isArray(model[key])) {
                fd.append(key, JSON.stringify(model[key]))
            }
            else {
                fd.append(key, model[key]);
            }
        }
        return fd;
    }

    return <section id="user">
            <div className="row">
                <div className="buttons">
                    <button onClick={ editModel }
                            disabled={ isEditing || isLoading } className="tb">
                            <span>Editer</span></button>
                    <button onClick={ e => saveModel('draft') }
                            disabled={ !isEditing || isLoading } className="tb">
                        <span>Enregistrer brouillon </span>
                        { (isLoading === 'draft') && <i className='fa fa-cog fa-spin'/> }
                    </button>
                    <button onClick={ e => saveModel('pending') }
                            disabled={ !isEditing || isLoading } className="tb">
                        <span>Demande de publication </span>
                        { (isLoading === 'pending') && <i className='fa fa-cog fa-spin'/> }
                    </button>
                    <button onClick={ cancelModel }
                            disabled={ !isEditing || isLoading } className="tb">
                        <span>Annuler</span></button>
                    <a href={THEME_CONST.SITE_URL + "?post_type=tb_trainees&p=" + model.id + "&preview=true"} target="_blank"><button className="tb">
                        <span>Prévisualiser</span></button></a>
                </div>

                <div className="col xl9 s12">
                    <h3 contentEditable={ isEditing }
                        onBlur={ e => setModel(model => ({...model, title: e.target.innerHTML})) }
                        dangerouslySetInnerHTML={ markup(model.title) }/>
                    <div className="single-img-container col l6 s12">
                        <div className={ isEditing && 'image-editable' }>
                            <img src={ model.thumbnailUrl || THEME_CONST.DEFAULT_IMG }
                                 onClick={ changeThumbnail }/>
                        </div>
                    </div>
                    <div className="col l6 s12">
                        <div>
                            <h6>Pourquoi cette formation ?</h6>
                            <div contentEditable={ isEditing }
                                 onBlur={ e => setModel(model => ({ ...model, questionTraining: e.target.innerHTML })) }
                                 dangerouslySetInnerHTML={ markup(model.questionTraining) }/>
                        </div>
                        <div>
                            <h6>Ce que j'aime dans ce métier ?</h6>
                            <div contentEditable={ isEditing }
                                 onBlur={ e => setModel(model => ({ ...model, questionJob1: e.target.innerHTML })) }
                                 dangerouslySetInnerHTML={ markup(model.questionJob1) }/>
                        </div>
                        <div>
                            <h6>Pourquoi il me correspond ?</h6>
                            <div contentEditable={ isEditing }
                                 onBlur={ e => setModel(model => ({ ...model, questionJob2: e.target.innerHTML })) }
                                 dangerouslySetInnerHTML={ markup(model.questionJob2) }/>
                        </div>
                        <div>
                            <h6>Ce que je peux apporter à une entreprise ?</h6>
                            <div contentEditable={ isEditing }
                                 onBlur={ e => setModel(model => ({ ...model, questionBusiness1: e.target.innerHTML })) }
                                 dangerouslySetInnerHTML={ markup(model.questionBusiness1) }/>
                        </div>
                        <div>
                            <h6>Ce que je cherche dans une entreprise ?</h6>
                            <div contentEditable={ isEditing }
                                 onBlur={ e => setModel(model => ({ ...model, questionBusiness2: e.target.innerHTML })) }
                                 dangerouslySetInnerHTML={ markup(model.questionBusiness2) }/>
                        </div>
                        <div>
                            <h6>Et aussi ... ?</h6>
                            <div contentEditable={ isEditing }
                                 onBlur={ e => setModel(model => ({ ...model, questionMore: e.target.innerHTML })) }
                                 dangerouslySetInnerHTML={ markup(model.questionMore) }/>
                        </div>
                    </div>
                    <div className="col s12">
                        <div className="col s12">
                            <h6>Curriculum Vitae</h6>
                                <label>
                                    { isEditing && <button className="tb" onClick={ changeCV }>Modifier le cv français</button> }
                                </label>
                                <div className="input-field">
                                    <input type="text" disabled={ true } value={ model.cvFileName }/>
                                </div>
                                <label>
                                    { isEditing && <button className="tb" onClick={ changeCVEn }>Modifier le cv anglais</button> }
                                </label>
                                <div className="input-field">
                                    <input type="text" disabled={ true } value={ model.cvEnFileName }/>
                                </div>
                        </div>
                        <div className="col l4 s12">
                            <h6>Technologies</h6>
                            <div>
                                <select id="skills"
                                        value={ model.skills }
                                        multiple={ true }
                                        disabled={ !isEditing }
                                        className="browser-default">
                                    { model.allSkills?.map(({term, color}) =>
                                        <option key={term.term_id} value={ term.term_id }>{ term.name }</option>
                                    ) }
                                </select>
                            </div>
                        </div>
                        <div className="col l4 s12">
                            <h6>Soft Skills</h6>
                            <div>
                                <select id="softSkills"
                                        value={ model.softSkills }
                                        multiple={ true }
                                        disabled={ !isEditing }
                                        className="browser-default">
                                    { model.allSoftSkills?.map(({term, color}) =>
                                        <option key={term.term_id} value={ term.term_id }>{ term.name }</option>
                                    ) }
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col s12">
                        <h6>Projet Professionnel</h6>
                        <div contentEditable={ isEditing }
                             onBlur={ e => setModel(model => ({ ...model, professionalProject: e.target.innerHTML })) }
                             dangerouslySetInnerHTML={ markup(model.professionalProject) }/>
                    </div>
                    <div className="col s12">
                        <h6>Réalisations</h6>
                        <div className="list">
                            { model.realisations.map(r => <Realisation item={r} isEditing={isEditing} itemChange={() => {
                                setModel(model => ({...model}))
                            }} itemDelete={ () => {
                                setModel(model => ({...model, realisations: model.realisations.filter(x => x !== r)}))
                            } } />) }
                        </div>
                        { isEditing && <button onClick={ e => {
                            setModel(model => ({...model, realisations: [...model.realisations, {}]}))
                        } } className="tb">Ajouter</button> }
                    </div>
                    <div className="col s12">
                        <h6>Experiences Professionelles</h6>
                        <div contentEditable={ isEditing }
                             onBlur={ e => setModel(model => ({ ...model, experiencesShortDescription: e.target.innerHTML })) }
                             dangerouslySetInnerHTML={ markup(model.experiencesShortDescription) }/>
                    </div>
                    <div className="col s12">
                        <h6><small>Détails</small></h6>
                        <div className="list">
                            { model.experiences.map(exp => <ListItem item={exp} isEditing={isEditing} itemChange={() => {
                                setModel(model => ({...model}))
                            }} itemDelete={ () => {
                                setModel(model => ({...model, experiences: model.experiences.filter(x => x !== exp)}))
                            } } />) }
                        </div>
                        { isEditing && <button onClick={ e => {
                            setModel(model => ({...model, experiences: [...model.experiences, {}]}))
                        } } className="tb">Ajouter</button> }
                    </div>
                    <div className="col s12">
                        <h6>Etudes et certifications</h6>
                        <div contentEditable={ isEditing }
                             onBlur={ e => setModel(model => ({ ...model, studiesShortDescription: e.target.innerHTML })) }
                             dangerouslySetInnerHTML={ markup(model.studiesShortDescription) }/>
                    </div>
                    <div className="col s12">
                        <h6><small>Détails</small></h6>
                        <div className="list">
                            { model.studies.map(st => <ListItem item={st} isEditing={isEditing} itemChange={() => {
                                setModel(model => ({...model}))
                            }} itemDelete={ () => {
                                setModel(model => ({...model, studies: model.studies.filter(x => x !== st)}))
                            } }/>) }
                        </div>
                        { isEditing && <button onClick={ e => {
                            setModel(model => ({...model, studies: [...model.studies, {}]}))
                        } } className="tb">Ajouter</button> }
                    </div>
                    <div className="col s12">
                        <h6>Centre d'intérêts</h6>
                        <p contentEditable={ isEditing }
                           onBlur={ e => setModel(model => ({...model, hobbies: e.target.innerHTML})) }
                           dangerouslySetInnerHTML={ markup(model.hobbies) }/>
                    </div>
                </div>
                <aside className="col xl3 s12">
                    <div>
                        <h5>Contacts</h5>
                        <div>
                            <div className="input-field">
                                <label><i className="fa fa-envelope"/></label>
                                <input type="email"
                                       disabled={ !isEditing }
                                       value={ model.email }
                                       onChange={ e => setModel(model => ({ ...model, email: e.target.value })) }/>
                            </div>
                            <div className="input-field">
                                <label><i className="fa fa-phone"/></label>
                                <input type="tel"
                                       disabled={ !isEditing }
                                       value={ model.phone }
                                       onChange={ e => setModel(model => ({ ...model, phone: e.target.value })) }/>
                            </div>
                            <div className="input-field">
                                <label><i className="fa fa-facebook"/></label>
                                <input type="url"
                                       disabled={ !isEditing }
                                       value={ model.facebook }
                                       onChange={ e => setModel(model => ({ ...model, facebook: e.target.value })) }/>
                            </div>
                            <div className="input-field">
                                <label><i className="fa fa-linkedin"/></label>
                                <input type="url"
                                       disabled={ !isEditing }
                                       value={ model.linkedin }
                                       onChange={ e => setModel(model => ({ ...model, linkedin: e.target.value })) }/>
                            </div>
                            <div className="input-field">
                                <label>LE FOREM</label>
                                <input type="url"
                                       disabled={ !isEditing }
                                       value={ model.forem }
                                       onChange={ e => setModel(model => ({ ...model, forem: e.target.value })) }/>
                                <div><a target="_blank" href="https://www.leforem.be/particuliers/placer-mon-profil-en-ligne.html">Créer un profil sur LeForem</a></div>
                            </div>
                        </div>
                        {/* <h5>Projets du stagiaire</h5>
                        <div>
                            <select id="projects"
                                    value={ model.projects }
                                    multiple={ true }
                                    disabled={ !isEditing }
                                    className="browser-default">
                                { model.allProjects?.map((p) =>
                                    <option value={ p.id } dangerouslySetInnerHTML={ markup(p.post_title) }/>
                                ) }
                            </select>
                        </div> */}
                    </div>
                </aside>
            </div>
        </section>
}

ReactDOM.render(<Profile model={THEME_DATA.model} />, document.getElementById('react-profile'));
