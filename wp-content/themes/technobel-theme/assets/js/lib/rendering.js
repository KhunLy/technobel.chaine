'use strict';

function renderItem(item, template) {
    for(let prop of Object.keys(item)){
        const regex = `({{)( *)(${prop})( *)(}})`;
        template = template.replaceAll(new RegExp(regex, 'g'), item[prop]);
    }
    return template;
}

function renderCollection(collection, template) {
    let collectionTemplate = '';
    for(let item of collection) {
        collectionTemplate += renderItem(item, template);
    }
    return collectionTemplate;
}