"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Loader = props => {
  var {
    isLoading
  } = props;
  return /*#__PURE__*/React.createElement("div", {
    className: 'loader' + (isLoading ? '' : ' hidden')
  }, /*#__PURE__*/React.createElement("i", {
    className: "cog cog-lg fa fa-cog"
  }), /*#__PURE__*/React.createElement("i", {
    className: "cog cog-counter cog-md fa fa-cog"
  }), /*#__PURE__*/React.createElement("i", {
    className: "cog cog-sm fa fa-cog"
  }));
};

var MoreBtn = props => {
  var {
    isLoading,
    disabled,
    text,
    onClick
  } = props;
  return /*#__PURE__*/React.createElement("div", {
    className: "more"
  }, /*#__PURE__*/React.createElement(Loader, {
    isLoading: isLoading
  }), /*#__PURE__*/React.createElement("button", {
    className: "tb",
    onClick: onClick,
    disabled: isLoading
  }, /*#__PURE__*/React.createElement("span", null, text, " "), isLoading && /*#__PURE__*/React.createElement("i", {
    className: "fa fa-cog fa-spin"
  })));
};

var Card = props => {
  var {
    model
  } = props;

  var markup = val => {
    return {
      __html: val
    };
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "col xl3 m6 s12"
  }, /*#__PURE__*/React.createElement("a", {
    href: model.link,
    className: "blk"
  }, /*#__PURE__*/React.createElement("div", {
    className: "tb-card"
  }, /*#__PURE__*/React.createElement("div", {
    className: "card-content"
  }, /*#__PURE__*/React.createElement("div", {
    className: "card-top"
  }, /*#__PURE__*/React.createElement("img", {
    src: model.image || THEME_CONST.DEFAULT_IMG,
    alt: "Card image cap"
  })), /*#__PURE__*/React.createElement("div", {
    className: "card-center"
  }, /*#__PURE__*/React.createElement("h5", {
    dangerouslySetInnerHTML: markup(model.title)
  }), /*#__PURE__*/React.createElement("p", {
    dangerouslySetInnerHTML: markup(model.excerpt)
  })), /*#__PURE__*/React.createElement("div", {
    className: "card-bottom"
  }, /*#__PURE__*/React.createElement("div", {
    className: "tags"
  }, model.skills.map(skill => /*#__PURE__*/React.createElement("div", {
    className: "tag skill",
    style: {
      color: skill.color,
      borderColor: skill.color
    }
  }, skill.value))))))));
};

var InfoCard = props => {
  var {
    model
  } = props;

  var markup = val => {
    return {
      __html: val
    };
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "col xl3 m6 s12"
  }, /*#__PURE__*/React.createElement("a", {
    href: model.link,
    className: "blk"
  }, /*#__PURE__*/React.createElement("div", {
    className: "tb-card"
  }, /*#__PURE__*/React.createElement("div", {
    className: "ribbon ribbon-top-left"
  }, /*#__PURE__*/React.createElement("span", null, "News")), /*#__PURE__*/React.createElement("div", {
    className: "card-content"
  }, /*#__PURE__*/React.createElement("div", {
    className: "card-top"
  }, /*#__PURE__*/React.createElement("img", {
    src: model.image || THEME_CONST.DEFAULT_IMG,
    alt: "Card image cap"
  })), /*#__PURE__*/React.createElement("div", {
    className: "card-center"
  }, /*#__PURE__*/React.createElement("h5", {
    dangerouslySetInnerHTML: markup(model.title)
  }), /*#__PURE__*/React.createElement("p", {
    dangerouslySetInnerHTML: markup(model.excerpt)
  }))))));
};

var TrainingCard = props => {
  var {
    model
  } = props;

  var markup = val => {
    return {
      __html: val
    };
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "col xl3 m6 s12"
  }, /*#__PURE__*/React.createElement("a", {
    href: model.link,
    className: "blk"
  }, /*#__PURE__*/React.createElement("div", {
    className: "tb-card card-training"
  }, /*#__PURE__*/React.createElement("div", {
    className: "card-content"
  }, /*#__PURE__*/React.createElement("div", {
    className: "card-top training-top",
    style: {
      'background-color': model.color
    }
  }, /*#__PURE__*/React.createElement("h5", {
    dangerouslySetInnerHTML: markup(model.title)
  }))))));
};

var Projects = props => {
  var {
    sticky
  } = props;
  var [isLoading, setIsLoading] = React.useState(false);
  var [projects, setProjects] = React.useState([]);
  var [disabled, sedDisabled] = React.useState(false);
  var [offset, setOffset] = React.useState(4);
  var limit = 4;

  var loadItems = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(function* () {
      setIsLoading(true);
      axios.get("".concat(THEME_CONST.API_URI, "api/projects?limit=").concat(limit, "&offset=").concat(offset, "&sticky=").concat(sticky)).then((_ref2) => {
        var {
          data
        } = _ref2;
        setProjects([...projects, ...data]);

        if (!data.length) {
          sedDisabled(true);
        }

        setOffset(offset + data.length);
        console.log(data);
      }).catch(error => {}).finally(() => {
        setIsLoading(false);
      });
    });

    return function loadItems() {
      return _ref.apply(this, arguments);
    };
  }();

  React.useEffect(() => {}, []);
  var items = /*#__PURE__*/React.createElement(React.Fragment, null, projects.map(item => item.type === 'post' ? /*#__PURE__*/React.createElement(Card, {
    model: item
  }) : /*#__PURE__*/React.createElement(InfoCard, {
    model: item
  })));
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "row"
  }, items), /*#__PURE__*/React.createElement(MoreBtn, {
    text: "+ de projets",
    onClick: loadItems,
    isLoading: isLoading,
    disabled: disabled
  }));
};

var Trainings = props => {
  var [isLoading, setIsLoading] = React.useState(false);
  var [projects, setProjects] = React.useState([]);
  var [disabled, sedDisabled] = React.useState(false);
  var [offset, setOffset] = React.useState(4);
  var limit = 4;

  var loadItems = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(function* () {
      setIsLoading(true);
      axios.get("".concat(THEME_CONST.API_URI, "api/trainings?limit=").concat(limit, "&offset=").concat(offset)).then((_ref4) => {
        var {
          data
        } = _ref4;
        setProjects([...projects, ...data]);

        if (!data.length) {
          sedDisabled(true);
        }

        setOffset(offset + data.length);
      }).catch(error => {}).finally(() => {
        setIsLoading(false);
      });
    });

    return function loadItems() {
      return _ref3.apply(this, arguments);
    };
  }();

  React.useEffect(() => {}, []);
  var items = /*#__PURE__*/React.createElement(React.Fragment, null, projects.map(item => /*#__PURE__*/React.createElement(TrainingCard, {
    model: item
  })));
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "row"
  }, items), /*#__PURE__*/React.createElement(MoreBtn, {
    text: "+ de fili\xE8res",
    onClick: loadItems,
    isLoading: isLoading,
    disabled: disabled
  }));
};

ReactDOM.render( /*#__PURE__*/React.createElement(Projects, {
  sticky: 0
}), document.getElementById('react-last-projects'));
ReactDOM.render( /*#__PURE__*/React.createElement(Projects, {
  sticky: 1
}), document.getElementById('react-sticky-projects'));
ReactDOM.render( /*#__PURE__*/React.createElement(Trainings, null), document.getElementById('react-last-trainings'));