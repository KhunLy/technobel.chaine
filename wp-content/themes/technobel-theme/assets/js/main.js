jQuery($ => {
    const $threebarElement = $('.threebar');
    const $sidePanelElement = $('#side-panel');
    const $headerElement = $('header');
    const $datePickerElements = document.querySelectorAll('.datepicker');
    const $selectElements = document.querySelectorAll('select');

    const datePickers = M.Datepicker.init($datePickerElements, {
        i18n: {
            cancel: 'Annuler',
            clear: 'Vider',
            done: 'Ok',
            months: [
                'Janvier',
                'Fevrier',
                'Mars',
                'Avril',
                'Mai',
                'Juin',
                'Juillet',
                'Août',
                'Septembere',
                'Octobre',
                'Novembre',
                'Decembre'
            ],
            monthsShort: [
                'Jan',
                'Fev',
                'Mar',
                'Avr',
                'Mai',
                'Jun',
                'Jui',
                'Aou',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            weekdays: [
                'Dimanche',
                'Lundi',
                'Mardi',
                'Mercredi',
                'Jeudi',
                'Vendredi',
                'Samedi'
            ],
            weekdaysShort: [
                'Dim',
                'Lun',
                'Mar',
                'Mer',
                'Jeu',
                'Ven',
                'Sam'
            ],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
        },
        format: 'dd/mm/yyyy'
    });
    const selects = M.FormSelect.init($selectElements);

    tippy('.tooltip', {
        content: ref => {
            return ref.getAttribute('data-content')
        },
        animation: 'scale-extreme',
        theme: 'tb'
    });

    $threebarElement.on('click', (event) => {
        $threebarElement.toggleClass('hamburger');
        $threebarElement.toggleClass('cross');
        $sidePanelElement.toggleClass('collapse')
    })

    let lastScrollTop = 0;
    window.addEventListener('scroll', () => {
        const st = window.pageYOffset || document.scrollTop;
        if (st > lastScrollTop){
            $headerElement.addClass('hidden');
        } else {
            $headerElement.removeClass('hidden');
        }
        lastScrollTop = st <= 0 ? 0 : st;
    }, false);

    document.addEventListener('paste', function (e) {
        e.preventDefault()
        var text = e.clipboardData.getData('text/plain')
        console.log(text);
        document.execCommand('insertHTML', false, text)
    })



});
