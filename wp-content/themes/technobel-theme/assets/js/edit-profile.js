"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ListItem = props => {
  var {
    item,
    itemChange,
    isEditing,
    itemDelete
  } = props;

  var markup = val => {
    return {
      __html: val
    };
  };

  var changeImage = () => {
    openFileDialog().then(files => {
      if (files.length) {
        item.image = files[0].url;
        itemChange();
      }
    });
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "list-item"
  }, /*#__PURE__*/React.createElement("div", {
    className: "img"
  }, /*#__PURE__*/React.createElement("img", {
    className: isEditing && 'image-editable',
    src: item.image || THEME_CONST.DEFAULT_IMG,
    onClick: changeImage
  })), /*#__PURE__*/React.createElement("div", {
    className: "info"
  }, /*#__PURE__*/React.createElement("h6", {
    contentEditable: isEditing,
    dangerouslySetInnerHTML: markup(item.title),
    onBlur: e => {
      item.title = e.target.innerHTML;
      itemChange();
    }
  }), /*#__PURE__*/React.createElement("p", {
    contentEditable: isEditing,
    dangerouslySetInnerHTML: markup(item.description),
    onBlur: e => {
      item.description = e.target.innerHTML;
      itemChange();
    }
  }), isEditing && /*#__PURE__*/React.createElement("p", {
    style: {
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement("a", {
    className: "red-text",
    style: {
      cursor: 'pointer'
    },
    onClick: itemDelete
  }, "Supprimer"))));
};

var Realisation = props => {
  var {
    item,
    itemChange,
    isEditing,
    itemDelete
  } = props;
  var [type, setType] = React.useState(item.type);
  var [url, setUrl] = React.useState(item.url);
  var [name, setName] = React.useState(item.name);
  var ref = React.createRef();
  React.useEffect(() => {
    //const typeSelect = M.FormSelect.init(ref.current);
    item.type = item.type || 'video';
  }, []);

  var changeFile = () => {
    var type = null;

    if (item.type === 'video' || item.type === 'interview') {
      type = ['video/mpg', 'video/mp4', 'video/mpeg'];
    }

    openFileDialog(type).then(files => {
      if (files.length) {
        item.url = files[0].url;
        itemChange();
        setUrl(files[0].url);
      }
    });
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "realisation"
  }, /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, "Nom"), /*#__PURE__*/React.createElement("input", {
    type: "text",
    value: name,
    onChange: e => {
      setName(e.target.value);
      item.name = e.target.value;
      itemChange();
    },
    readOnly: !isEditing
  })), /*#__PURE__*/React.createElement("div", {
    className: "type",
    style: {
      paddingBottom: '10px'
    }
  }, /*#__PURE__*/React.createElement("select", {
    ref: ref,
    value: item.type,
    onChange: e => {
      setType(e.target.value);
      setUrl(null);
      item.type = e.target.value;
      itemChange();
    },
    disabled: !isEditing,
    className: "browser-default"
  }, /*#__PURE__*/React.createElement("option", {
    value: "video"
  }, "Video"), /*#__PURE__*/React.createElement("option", {
    value: "interview"
  }, "Interview"), /*#__PURE__*/React.createElement("option", {
    value: "document"
  }, "Document"), /*#__PURE__*/React.createElement("option", {
    value: "website"
  }, "Site Web"), /*#__PURE__*/React.createElement("option", {
    value: "git"
  }, "Lien git"), /*#__PURE__*/React.createElement("option", {
    value: "cube"
  }, "Cube"))), /*#__PURE__*/React.createElement("div", {
    className: "attachment"
  }, type !== null && /*#__PURE__*/React.createElement(React.Fragment, null, type !== 'website' && type !== 'git' && /*#__PURE__*/React.createElement(React.Fragment, null, isEditing && /*#__PURE__*/React.createElement("label", null, /*#__PURE__*/React.createElement("button", {
    onClick: changeFile,
    className: "tb inverse"
  }, "Parcourir")), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("input", {
    readOnly: true,
    value: url
  }))), (type === 'website' || type === 'git') && /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, "Url"), /*#__PURE__*/React.createElement("input", {
    type: "url",
    value: url,
    onChange: e => {
      setUrl(e.target.value);
      item.url = e.target.value;
      itemChange();
    },
    readOnly: !isEditing
  })))), isEditing && /*#__PURE__*/React.createElement("p", {
    style: {
      textAlign: 'right'
    }
  }, /*#__PURE__*/React.createElement("a", {
    className: "red-text",
    style: {
      cursor: 'pointer'
    },
    onClick: itemDelete
  }, "Supprimer")));
};

var Profile = props => {
  var _model$allSkills, _model$allSoftSkills;

  var [isLoading, setIsLoading] = React.useState(null);
  var [isEditing, setIsEditing] = React.useState(false);
  var [oldValues, setOldValues] = React.useState(null);
  var [model, setModel] = React.useState(props.model);
  React.useEffect(() => {
    // init
    var skillsSelect = jQuery('#skills').select2();
    var softSkillsSelect = jQuery('#softSkills').select2();
    var projectsSelect = jQuery('#projects').select2();
    skillsSelect.on('change', skillsChanged);
    softSkillsSelect.on('change', softSkillsChanged);
    projectsSelect.on('change', projectsChanged);
  }, []);

  var markup = val => {
    return {
      __html: val
    };
  };

  var editModel = () => {
    setOldValues(_objectSpread(_objectSpread({}, model), {}, {
      studies: model.studies.map(x => _objectSpread({}, x)),
      experiences: model.experiences.map(x => _objectSpread({}, x)),
      projects: [...model.projects],
      skills: [...model.skills],
      softSkills: [...model.softSkills]
    }));
    setIsEditing(true);
  };

  var saveModel = status => {
    setIsLoading(status);
    axios.post("".concat(THEME_CONST.API_URI, "api/trainees"), getFormData(status), {
      headers: {
        'content-type': 'application/json',
        'X-WP-Nonce': THEME_CONST.NONCE
      }
    }).then((_ref) => {
      var {
        data
      } = _ref;
      toastr.success('Sauvergarde réussie');
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        id: data
      }));
      setIsEditing(false);
    }).catch(e => {
      toastr.error('Une erreur est survenue');
    }).finally(() => {
      setIsLoading(null);
    });
  };

  var cancelModel = () => {
    jQuery('#skills').val(oldValues.skills).trigger('change');
    jQuery('#softSkills').val(oldValues.softSkills).trigger('change');
    jQuery('#projects').val(oldValues.projects).trigger('change');
    setModel(oldValues);
    setIsEditing(false);
  };

  var changeThumbnail = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(function* (e) {
      var files = yield openFileDialog();

      if (files.length) {
        setModel(model => _objectSpread(_objectSpread({}, model), {}, {
          thumbnailUrl: files[0].url,
          thumbnail: files[0].id
        }));
      }
    });

    return function changeThumbnail(_x) {
      return _ref2.apply(this, arguments);
    };
  }();

  var changeCV = /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator(function* (e) {
      var files = yield openFileDialog('application/pdf');

      if (files.length) {
        setModel(model => _objectSpread(_objectSpread({}, model), {}, {
          cvUrl: files[0].url,
          cvFileName: files[0].name,
          cv: files[0].id
        }));
      }
    });

    return function changeCV(_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  var changeCVEn = /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(function* (e) {
      var files = yield openFileDialog('application/pdf');

      if (files.length) {
        setModel(model => _objectSpread(_objectSpread({}, model), {}, {
          cvEnUrl: files[0].url,
          cvEnFileName: files[0].name,
          cvEn: files[0].id
        }));
      }
    });

    return function changeCVEn(_x3) {
      return _ref4.apply(this, arguments);
    };
  }();

  var skillsChanged = e => {
    var selection = jQuery(e.target).select2('data').map(item => item.id);
    setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      skills: selection
    }));
  };

  var softSkillsChanged = e => {
    var selection = jQuery(e.target).select2('data').map(item => item.id);
    setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      softSkills: selection
    }));
  };

  var projectsChanged = e => {
    var selection = jQuery(e.target).select2('data').map(item => item.id);
    setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      projects: selection
    }));
  };

  var getFormData = status => {
    var fd = new FormData();
    fd.append('postStatus', status);

    for (var key of Object.keys(model).filter(k => !['allSkills', 'allSoftSkills', 'allProjects'].includes(k) && model[k] != null)) {
      if (Array.isArray(model[key])) {
        fd.append(key, JSON.stringify(model[key]));
      } else {
        fd.append(key, model[key]);
      }
    }

    return fd;
  };

  return /*#__PURE__*/React.createElement("section", {
    id: "user"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row"
  }, /*#__PURE__*/React.createElement("div", {
    className: "buttons"
  }, /*#__PURE__*/React.createElement("button", {
    onClick: editModel,
    disabled: isEditing || isLoading,
    className: "tb"
  }, /*#__PURE__*/React.createElement("span", null, "Editer")), /*#__PURE__*/React.createElement("button", {
    onClick: e => saveModel('draft'),
    disabled: !isEditing || isLoading,
    className: "tb"
  }, /*#__PURE__*/React.createElement("span", null, "Enregistrer brouillon "), isLoading === 'draft' && /*#__PURE__*/React.createElement("i", {
    className: "fa fa-cog fa-spin"
  })), /*#__PURE__*/React.createElement("button", {
    onClick: e => saveModel('pending'),
    disabled: !isEditing || isLoading,
    className: "tb"
  }, /*#__PURE__*/React.createElement("span", null, "Demande de publication "), isLoading === 'pending' && /*#__PURE__*/React.createElement("i", {
    className: "fa fa-cog fa-spin"
  })), /*#__PURE__*/React.createElement("button", {
    onClick: cancelModel,
    disabled: !isEditing || isLoading,
    className: "tb"
  }, /*#__PURE__*/React.createElement("span", null, "Annuler")), /*#__PURE__*/React.createElement("a", {
    href: THEME_CONST.SITE_URL + "?post_type=tb_trainees&p=" + model.id + "&preview=true",
    target: "_blank"
  }, /*#__PURE__*/React.createElement("button", {
    className: "tb"
  }, /*#__PURE__*/React.createElement("span", null, "Pr\xE9visualiser")))), /*#__PURE__*/React.createElement("div", {
    className: "col xl9 s12"
  }, /*#__PURE__*/React.createElement("h3", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      title: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.title)
  }), /*#__PURE__*/React.createElement("div", {
    className: "single-img-container col l6 s12"
  }, /*#__PURE__*/React.createElement("div", {
    className: isEditing && 'image-editable'
  }, /*#__PURE__*/React.createElement("img", {
    src: model.thumbnailUrl || THEME_CONST.DEFAULT_IMG,
    onClick: changeThumbnail
  }))), /*#__PURE__*/React.createElement("div", {
    className: "col l6 s12"
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h6", null, "Pourquoi cette formation ?"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      questionTraining: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.questionTraining)
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h6", null, "Ce que j'aime dans ce m\xE9tier ?"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      questionJob1: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.questionJob1)
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h6", null, "Pourquoi il me correspond ?"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      questionJob2: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.questionJob2)
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h6", null, "Ce que je peux apporter \xE0 une entreprise ?"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      questionBusiness1: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.questionBusiness1)
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h6", null, "Ce que je cherche dans une entreprise ?"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      questionBusiness2: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.questionBusiness2)
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h6", null, "Et aussi ... ?"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      questionMore: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.questionMore)
  }))), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Curriculum Vitae"), /*#__PURE__*/React.createElement("label", null, isEditing && /*#__PURE__*/React.createElement("button", {
    className: "tb",
    onClick: changeCV
  }, "Modifier le cv fran\xE7ais")), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    disabled: true,
    value: model.cvFileName
  })), /*#__PURE__*/React.createElement("label", null, isEditing && /*#__PURE__*/React.createElement("button", {
    className: "tb",
    onClick: changeCVEn
  }, "Modifier le cv anglais")), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("input", {
    type: "text",
    disabled: true,
    value: model.cvEnFileName
  }))), /*#__PURE__*/React.createElement("div", {
    className: "col l4 s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Technologies"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("select", {
    id: "skills",
    value: model.skills,
    multiple: true,
    disabled: !isEditing,
    className: "browser-default"
  }, (_model$allSkills = model.allSkills) === null || _model$allSkills === void 0 ? void 0 : _model$allSkills.map((_ref5) => {
    var {
      term,
      color
    } = _ref5;
    return /*#__PURE__*/React.createElement("option", {
      key: term.term_id,
      value: term.term_id
    }, term.name);
  })))), /*#__PURE__*/React.createElement("div", {
    className: "col l4 s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Soft Skills"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("select", {
    id: "softSkills",
    value: model.softSkills,
    multiple: true,
    disabled: !isEditing,
    className: "browser-default"
  }, (_model$allSoftSkills = model.allSoftSkills) === null || _model$allSoftSkills === void 0 ? void 0 : _model$allSoftSkills.map((_ref6) => {
    var {
      term,
      color
    } = _ref6;
    return /*#__PURE__*/React.createElement("option", {
      key: term.term_id,
      value: term.term_id
    }, term.name);
  }))))), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Projet Professionnel"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      professionalProject: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.professionalProject)
  })), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, "R\xE9alisations"), /*#__PURE__*/React.createElement("div", {
    className: "list"
  }, model.realisations.map(r => /*#__PURE__*/React.createElement(Realisation, {
    item: r,
    isEditing: isEditing,
    itemChange: () => {
      setModel(model => _objectSpread({}, model));
    },
    itemDelete: () => {
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        realisations: model.realisations.filter(x => x !== r)
      }));
    }
  }))), isEditing && /*#__PURE__*/React.createElement("button", {
    onClick: e => {
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        realisations: [...model.realisations, {}]
      }));
    },
    className: "tb"
  }, "Ajouter")), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Experiences Professionelles"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      experiencesShortDescription: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.experiencesShortDescription)
  })), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, /*#__PURE__*/React.createElement("small", null, "D\xE9tails")), /*#__PURE__*/React.createElement("div", {
    className: "list"
  }, model.experiences.map(exp => /*#__PURE__*/React.createElement(ListItem, {
    item: exp,
    isEditing: isEditing,
    itemChange: () => {
      setModel(model => _objectSpread({}, model));
    },
    itemDelete: () => {
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        experiences: model.experiences.filter(x => x !== exp)
      }));
    }
  }))), isEditing && /*#__PURE__*/React.createElement("button", {
    onClick: e => {
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        experiences: [...model.experiences, {}]
      }));
    },
    className: "tb"
  }, "Ajouter")), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Etudes et certifications"), /*#__PURE__*/React.createElement("div", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      studiesShortDescription: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.studiesShortDescription)
  })), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, /*#__PURE__*/React.createElement("small", null, "D\xE9tails")), /*#__PURE__*/React.createElement("div", {
    className: "list"
  }, model.studies.map(st => /*#__PURE__*/React.createElement(ListItem, {
    item: st,
    isEditing: isEditing,
    itemChange: () => {
      setModel(model => _objectSpread({}, model));
    },
    itemDelete: () => {
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        studies: model.studies.filter(x => x !== st)
      }));
    }
  }))), isEditing && /*#__PURE__*/React.createElement("button", {
    onClick: e => {
      setModel(model => _objectSpread(_objectSpread({}, model), {}, {
        studies: [...model.studies, {}]
      }));
    },
    className: "tb"
  }, "Ajouter")), /*#__PURE__*/React.createElement("div", {
    className: "col s12"
  }, /*#__PURE__*/React.createElement("h6", null, "Centre d'int\xE9r\xEAts"), /*#__PURE__*/React.createElement("p", {
    contentEditable: isEditing,
    onBlur: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      hobbies: e.target.innerHTML
    })),
    dangerouslySetInnerHTML: markup(model.hobbies)
  }))), /*#__PURE__*/React.createElement("aside", {
    className: "col xl3 s12"
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h5", null, "Contacts"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, /*#__PURE__*/React.createElement("i", {
    className: "fa fa-envelope"
  })), /*#__PURE__*/React.createElement("input", {
    type: "email",
    disabled: !isEditing,
    value: model.email,
    onChange: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      email: e.target.value
    }))
  })), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, /*#__PURE__*/React.createElement("i", {
    className: "fa fa-phone"
  })), /*#__PURE__*/React.createElement("input", {
    type: "tel",
    disabled: !isEditing,
    value: model.phone,
    onChange: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      phone: e.target.value
    }))
  })), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, /*#__PURE__*/React.createElement("i", {
    className: "fa fa-facebook"
  })), /*#__PURE__*/React.createElement("input", {
    type: "url",
    disabled: !isEditing,
    value: model.facebook,
    onChange: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      facebook: e.target.value
    }))
  })), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, /*#__PURE__*/React.createElement("i", {
    className: "fa fa-linkedin"
  })), /*#__PURE__*/React.createElement("input", {
    type: "url",
    disabled: !isEditing,
    value: model.linkedin,
    onChange: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      linkedin: e.target.value
    }))
  })), /*#__PURE__*/React.createElement("div", {
    className: "input-field"
  }, /*#__PURE__*/React.createElement("label", null, "LE FOREM"), /*#__PURE__*/React.createElement("input", {
    type: "url",
    disabled: !isEditing,
    value: model.forem,
    onChange: e => setModel(model => _objectSpread(_objectSpread({}, model), {}, {
      forem: e.target.value
    }))
  }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("a", {
    target: "_blank",
    href: "https://www.leforem.be/particuliers/placer-mon-profil-en-ligne.html"
  }, "Cr\xE9er un profil sur LeForem"))))))));
};

ReactDOM.render( /*#__PURE__*/React.createElement(Profile, {
  model: THEME_DATA.model
}), document.getElementById('react-profile'));