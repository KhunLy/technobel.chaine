jQuery($ => {
    const $openModalElements = $('.open-modal');

    const $modalElements = $('.modal');

    const $closeElements = $('.close');

    const $tableElements = $('table');

    $openModalElements.on('click', e => {
        const target = $(e.target).attr('data-target');
        $('#' + target).css({ display: 'block' })
    });

    $closeElements.on('click', e => {
        $modalElements.css({ display: 'none' });
    })


    $tableElements.DataTable({
        paging: false,
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
        },
        order: [[ 0, "asc" ]],
        columnDefs: [
            { orderable: false, targets: 2 }
        ],
    });
});