jQuery($ => {

    const months = ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Dec'];

    const $tableElement = $('#table');

    const dt = $tableElement.DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'csv'
        ],
        language: { url: 'https://cdn.datatables.net/plug-ins/1.10.24/i18n/French.json' }
    });

    let selectedYear = 2021;

    const chart1 = Highcharts.chart('chart1', {

        chart: {
            type: 'spline'
        },

        title: {
            text: 'Evolution des contacts et partages en ' + selectedYear + ' par mois et par type'
        },

        xAxis: {
            categories: months,
            labels: {
                rotation: 45
            }
        },

        yAxis: {
            title: {
                text: 'Quantité'
            }
        },

        series: [
            { name: 'Facebook', data: [0,0,0,0,0,0,0,0,0,0,0,0] },
            { name: 'Email', data: [0,0,0,0,0,0,0,0,0,0,0,0] },
            { name: 'Phone', data: [0,0,0,0,0,0,0,0,0,0,0,0] },
            { name: 'LinkedIn', data: [0,0,0,0,0,0,0,0,0,0,0,0] },
            { name: 'Print', data: [0,0,0,0,0,0,0,0,0,0,0,0] },
        ],

    });

    const chart2 = Highcharts.chart('chart2', {

        chart: {
            type: 'pie'
        },

        title: {
            text: 'Répartition par type en 2021'
        },

        series: [{
            name: 'Type',
            colorByPoint: true,
            data: []
        }]

    });

    const chart3 = Highcharts.chart('chart3', {

        chart: {
            type: 'column'
        },

        title: {
            text: 'Répartition par filière en 2021'
        },

        series: []

    });

    axios.get(`${THEME_CONST.API_URI}api/stats/click`).then(({data}) => {

        console.log(data);

        const types = ['email', 'phone', 'facebook', 'linkedin', 'print']

        for (let type of types) {
            const newData = [0,0,0,0,0,0,0,0,0,0,0,0];
            const serie = chart1.series.find(s => s.name.toLowerCase() === type);
            for (item of data.filter(x => x.type === type)) {
                newData[item.month - 1] += parseInt(item.total);
                serie.setData(newData)
            }
        }

        const chart2Data = [];
        for (let type of types) {
            let sum = 0;
            for (item of data.filter(x => x.type === type)) {
                sum += parseInt(item.total);
            }
            chart2Data.push({ name: type.toUpperCase(), y: sum })
        }

        const pieSerie = chart2.series.find(s => s.name.toLowerCase() === 'type');
        pieSerie.setData(chart2Data);

        const chart3Data = [];
        for (let result of data) {
            let toUpdate = chart3Data.find(d => d.name === result.training);
            if (!toUpdate) {
                toUpdate = { name: result.training, data: [parseInt(result.total)] };
                chart3Data.push(toUpdate);
            }
            else {
                toUpdate.data[0] += parseInt(result.total);
            }
        }

        for(let serie of chart3Data) {
            chart3.addSeries(serie);
        }

        for(let result of data) {
            dt.row.add([
                result.month /*+ ' (' +  months[parseInt(result.month) - 1] + ')'*/,
                result.type,
                result.training,
                result.total
            ]).draw();
        }
    });
});