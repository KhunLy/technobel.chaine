<?php
function getToken() {
    try {
        $url = 'https://api.badgr.io/o/token';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = "username=lykhun@gmail.com&password=technobel2021";

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);


        $resp = curl_exec($curl);
        curl_close($curl);


        $data = json_decode($resp);
        return $data->access_token;

    } catch (Exception $e) {
        return null;
    }
}

function getEntityIds($token) {
    try {
        $url = 'https://api.badgr.io/v2/issuers';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Authorization: Bearer " . $token,
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);


        $resp = curl_exec($curl);
        curl_close($curl);


        $data = json_decode($resp);
        return array_map(function($item) {return $item->entityId;}, $data->result);

    } catch (Exception $e) {
        return null;
    }
}

function getBadges($token, $entityIds, $email) {
    try {
		$datas = [];
		foreach($entityIds as $entityId) {
			$url = 'https://api.badgr.io/v2/issuers/'.$entityId.'/assertions?recipient='.$email;

			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

			$headers = array(
				"Authorization: Bearer " . $token,
			);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);


			$resp = curl_exec($curl);
			curl_close($curl);

			
			$data = json_decode($resp);
			
			$datas = array_merge($datas, $data->result);
		}
        return $datas;

    } catch (Exception $e) {
        return null;
    }
}

$token = getToken();
$entityIds = getEntityIds($token);

$badges = getBadges($token, $entityIds, get_field('email'));


//$badges = get_field('badges');
$title = get_the_title();
$thumbnail = get_post_thumbnail_id();
$questionTraining = get_field('question_training');
$questionJob1 = get_field('question_job_1');
$questionJob2 = get_field('question_job_2');
$questionBusiness1 = get_field('question_business_1');
$questionBusiness2 = get_field('question_business_2');
$questionMore = get_field('question_more');
$professionalProject = get_field('professional_project');
$experiencesShortDescription = get_field('experiences_short_description');
$studiesShortDescription = get_field('studies_short_description');
$hobbies = get_field('hobbies');
$skills = get_the_terms(get_the_ID(), 'tb_skills');
$softSkills = get_the_terms(get_the_ID(), 'tb_soft_skills');
$experiences = json_decode(get_post_meta(get_the_ID(), 'experiences', true),false);
$studies = json_decode(get_post_meta(get_the_ID(), 'studies', true), false);
$realisations = json_decode(get_post_meta(get_the_ID(), 'realisations', true), false) ?: [];
$projects = array_filter(get_field('projects') ?: [], function($item){
    return $item->post_status === 'publish';
});

$training = get_field('training');

$emailAttr = !get_field('email') ? 'class="disabled"'
    : '';

$phoneAttr = !get_field('phone') ? 'class="disabled"'
    : 'class="tooltip share" data-type="phone" data-trainee="' . get_the_ID() . '" data-training="' . $training->ID . '" data-content="'. get_field('phone') . '" href="tel:' .get_field('phone'). '"';

$linkedinAttr = !get_field('linkedin') ? 'class="disabled"'
    : 'class="tooltip" data-content="'. get_field('linkedin') . '" target="_blank" href="' .get_field('linkedin'). '"';

$facebookAttr = !get_field('facebook') ? 'class="disabled"'
    : 'class="tooltip" data-content="'. get_field('facebook') . '" target="_blank" href="' .get_field('facebook'). '"';

$instagramAttr = !get_field('instagram') ? 'class="disabled"'
    : 'class="tooltip" data-content="'. get_field('instagram') . '" target="_blank" href="' .get_field('instagram'). '"';

$foremAttr = 'class="tooltip" data-content="'. get_field('forem') . '" target="_blank" href="https://leforem.be/espace-entreprise"';
global $post;
$authorId = $post->post_author;
$post_status = $post->post_status;
?>
<?php get_header() ?>

<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>

<div id="modal-email" class="modal">
    <form>
        <div class="modal-content">
            <h4>Envoyer un message à <?php the_title() ?></h4>
            <div class="tb-blue input-field">
                <label for="email">Votre email (*)</label>
                <input required class="validate" id="email" name="email" type="email">
            </div>
            <div class="tb-blue input-field">
                <label for="subject">Sujet de votre message (*)</label>
                <input required class="validate" id="subject" name="subject" type="text">
            </div>
            <div class="textarea-container">
                <label for="content">Contenu de votre message</label>
                <textarea class="validate" id="content" name="content"></textarea>
            </div>
        </div>
        <div class="modal-footer" style="padding: 0 24px 24px 24px">
            <button type="button" class="modal-close tb inverse">Annuler</button>
            <button id="send-email" class="share tb" data-type="email" data-trainee="<?= get_the_ID() ?>" data-training="<?= $training->ID ?>" data-to="<?= get_field('email') ?>" disabled>Envoyer <i class="fas fa-paper-plane"></i></button>
        </div>
    </form>
</div>

<section id="user">
    <div class="row">
        <div class="col xl9 s12">
            <h3 id="field-title"><?php the_title() ?></h3>
            <?php if(get_field('employed')): ?><h3><small> À l'emploi</small></h3><?php endif?>
            <div class="single-img-container col l6 s12">
                <img id="field-thumbnail" src="<?php the_post_thumbnail_url(); ?>">
                <div class="socials-share print-hidden">
                    <button class="print-button share" data-type="print" data-trainee="<?= get_the_ID() ?>" data-training="<?= $training->ID ?>"><i class="fa fa-print"></i> Imprimer</button>
                        <a target="_blank" href="<?= 'https://www.facebook.com/dialog/share?app_id=482065732919932&display=popup&href=' . get_the_permalink()?>">
                        <button class="fb-button share" data-type="facebook" data-trainee="<?= get_the_ID() ?>" data-training="<?= $training->ID ?>"><i class="fa fa-facebook"></i> Partager</button>
                    </a>
                        <a target="_blank" href="<?= 'http://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink() ?>">
                        <button class="linkedin-button share" data-type="linkedin" data-trainee="<?= get_the_ID() ?>" data-training="<?= $training->ID ?>"><i class="fa fa-linkedin"></i> Partager</button>
                    </a>
                </div>
            </div>
            <div class="col only-print">
                <h6>Infos</h6>
                <div class="col"> 
                    <p><i class="fa fa-envelope"></i> <?php the_field('email'); ?></p>
                    <p><i class="fa fa-phone"></i> <?php the_field('phone'); ?></p>
                    <p><i class="fa fa-linkedin"></i> <?php the_field('linkedin'); ?></p>
                </div>
                <div class="col">
                    <p><i class="fa fa-facebook"></i> <?php the_field('facebook'); ?></p>
                    <p><i class="fa fa-instagram"></i> <?php the_field('instagram'); ?></p>
                    <p><span class="forem-print">LE FOREM</span> <?php the_field('forem'); ?></p>
                </div>
            </div>
            <div class="col l6 s12">
                <?php if($questionTraining):?>
                    <div id="field-question_training">
                        <h6>Pourquoi cette formation ?</h6>
                        <?php the_field('question_training'); ?>
                    </div>
                <?php endif ?>
                <?php if($questionJob1):?>
                <div id="field-question_job_1">
                    <h6>Ce que j'aime dans ce métier ?</h6>
                    <?php the_field('question_job_1'); ?>
                </div>
                <?php endif ?>
                <?php if($questionJob2):?>
                <div id="field-question_job_2">
                    <h6>Pourquoi il me correspond ?</h6>
                    <?php the_field('question_job_2'); ?>
                </div>
                <?php endif ?>
                <?php if($questionBusiness1):?>
                <div id="field-question_business_1">
                    <h6>Ce que je peux apporter à une entreprise ?</h6>
                    <?php the_field('question_business_1'); ?>
                </div>
                <?php endif ?>
                <?php if($questionBusiness2):?>
                <div id="field-question_business_2">
                    <h6>Ce que je cherche dans une entreprise ?</h6>
                    <?php the_field('question_business_2'); ?>
                </div>
                <?php endif ?>
                <?php if($questionMore):?>
                <div id="field-question_more">
                    <h6>Et aussi ...</h6>
                    <?php the_field('question_more'); ?>
                </div>
                <?php endif ?>
            </div>
            <div class="col s12 print-hidden">
                <?php if(get_field('cv') || get_field('cv_en')):?>
                    <h6>Curriculum Vitae</h6>
                    <?php if(get_field('cv')):?>
                        <a class="blk mb-1" href="<?= get_field('cv')['url']; ?>" target="_blank">
                            <button class="tb share" data-type="download" data-trainee="<?= get_the_ID() ?>" data-training="<?= $training->ID ?>">
                                <i class="fa fa-download"></i>
                                <span> télécharger en français</span>
                            </button>
                        </a>
                    <?php endif; ?>
                    <?php if(get_field('cv_en')):?>
                        <a class="blk mb-1" href="<?= get_field('cv_en')['url']; ?>" target="_blank">
                            <button class="tb share" data-type="download" data-trainee="<?= get_the_ID() ?>" data-training="<?= $training->ID ?>">
                                <i class="fa fa-download"></i>
                                <span> télécharger en anglais</span>
                            </button>
                        </a>
                    <?php endif;?>
                <?php endif;?>
            </div>
            <div class="col s12">
                <h6>Badges</h6>
                <div class="badges">
                <?php if($badges): ?>
                    <?php foreach( $badges as $badge ): ?>
                        <a href="<?= $badge->openBadgeId ?>" target="_blank"><img src="<?= $badge->image ?>"></a>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p>Pas encore de badge</p>
                <?php endif; ?>
                </div>
            </div>
            <div class="col s12">
                <h6>Technologies</h6>
                <div class="tags">
                    <?php if($skills): ?>
                        <?php foreach($skills as $skill): ?>
                            <?php $color = get_field('color', $skill) ?>
                            <div class="tag skill" style="border-color: <?= $color ?>; color: <?= $color ?>">
                                <?= $skill->name ?>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <p>Pas encore de technologies</p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col s12">
                <h6>Soft Skills</h6>
                <div class="tags">
                    <?php if($skills): ?>
                        <?php foreach($softSkills as $skill): ?>
                            <?php $color = get_field('color', $skill) ?>
                            <div class="tag category" style="background-color: <?= $color ?>">
                                <?= $skill->name ?>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <p>Pas encore de "soft skill"</p>
                    <?php endif; ?>
                </div>
            </div>
            <div id="field-professional_project" class="col s12">
                <h6>Projet Professionnel</h6>
                <?php the_field('professional_project'); ?>
            </div>
            <div class="col s12 print-hidden">
                <h6>Réalisations</h6>
                    <?php if($realisations && count($realisations) > 0): ?>
                        <div class="row">
                            <?php $nbByCol = floor(count($realisations) / 3) + (count($realisations) % 3 == 0 ? 0 : 1); $i = 0; ?>
                            <div class="gallery">
                            <?php foreach ($realisations as $r): ?>
                                <?php if($i % $nbByCol == 0): ?>
                                    <div class="gallery__column">
                                <?php endif; ?>
                                    <?php if(($r->type === 'video' || $r->type === 'interview') && $r->url){ ?>

                                        <div class="outer">
                                            <video class="inner" controls>
                                                <source src="<?= $r->url ?>">
                                            </video>
                                            <figcaption class="gallery__caption"><?= $r->name ?></figcaption>
                                        </div>

                                    <?php } elseif($r->type === 'document' || $r->type === 'cube') { ?>
                                        <?php if(str_ends_with($r->url, '.pdf')) { ?>

                                            <a class="outer" href="<?= $r->url ?>" target="_blank">
                                                <figure class="gallery__thumb">
                                                    <canvas class="inner" style="border:1px solid black; width: 100%;" id="canvas_<?=$i?>"></canvas>
                                                    <script>
                                                        // If absolute URL from the remote server is provided, configure the CORS
                                                        // header on that server.
                                                        var url = '<?= $r->url ?>';

                                                        // Loaded via <script> tag, create shortcut to access PDF.js exports.
                                                        var pdfjsLib = window['pdfjs-dist/build/pdf'];

                                                        // The workerSrc property shall be specified.
                                                        pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

                                                        // Asynchronous download of PDF
                                                        var loadingTask = pdfjsLib.getDocument(url);
                                                        loadingTask.promise.then(function(pdf) {
                                                            console.log('PDF loaded');

                                                            // Fetch the first page
                                                            var pageNumber = 1;
                                                            pdf.getPage(pageNumber).then(function(page) {
                                                                console.log('Page loaded');

                                                                var scale = 0.5;
                                                                var viewport = page.getViewport({scale: scale});

                                                                // Prepare canvas using PDF page dimensions
                                                                var canvas = document.getElementById('canvas_<?=$i?>');
                                                                canvas.height = viewport.height;
                                                                canvas.width = viewport.width;
                                                                var context = canvas.getContext('2d');

                                                                // Render PDF page into canvas context
                                                                var renderContext = {
                                                                    canvasContext: context,
                                                                    viewport: viewport
                                                                };
                                                                var renderTask = page.render(renderContext);
                                                                renderTask.promise.then(function () {
                                                                    console.log('Page rendered');
                                                                });
                                                            });
                                                        }, function (reason) {
                                                            // PDF loading error
                                                            console.error(reason);
                                                        });
                                                    </script>
                                                    <figcaption class="gallery__caption"><?= $r->name ?></figcaption>
                                                </figure>
                                            </a>

                                        <?php } elseif(str_ends_with($r->url, '.png') || str_ends_with($r->url, '.jpg') || str_ends_with($r->url, '.jpeg')) { ?>

                                            <a class="outer" href="<?= $r->url ?>" target="_blank">
                                                <figure class="gallery__thumb">
                                                    <img class="inner" src="<?= $r->url ?>">
                                                    <figcaption class="gallery__caption"><?= $r->name ?></figcaption>
                                                </figure>
                                            </a>

                                        <?php } else { ?>

                                            <a class="outer" download href="<?= $r->url ?>">
                                                <figure class="gallery__thumb">
                                                    <img class="inner" src="<?= THEME_ASSETS_IMAGE_URI . ($r->type === 'cube' ? '/cube.jpg' :'/file.jpg') ?>">
                                                    <figcaption class="gallery__caption"><?= $r->name ?></figcaption>
                                                </figure>
                                            </a>

                                        <?php } ?>

                                    <?php } elseif($r->type === 'website' || $r->type === 'git') { ?>
                                        <a class="outer" target="_blank" href="<?= $r->url ?>">
                                            <figure class="gallery__thumb">
                                                <img class="inner" src="<?= THEME_ASSETS_IMAGE_URI .'/' . $r->type . '.jpg' ?>">
                                                <figcaption class="gallery__caption"><?= $r->name ?></figcaption>
                                            </figure>
                                        </a>
                                    <?php } ?>
                                <?php $i++; ?>
                                <?php if($i % $nbByCol == 0 || $i == count($realisations)): ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <p>Pas encore de réalisations</p>
                    <?php endif;?>
            </div>
            <div id="field-experiences_short_description" class="col s12">
                <h6>Experiences Professionelles</h6>
                <?php the_field('experiences_short_description'); ?>
            </div>
            <?php if($experiences): ?>
            <div class="col s12">
                <h6><small>Détails</small></h6>
                <div class="list">
                    <?php foreach ($experiences ?: [] as $exp) :?>
                        <div class="list-item">
                            <div class="img">
                                <img src="<?= $exp->image ?: THEME_DEFAULT_IMG ; ?>">
                            </div>
                            <div class="info">
                                <h6><?= $exp->title ?></h6>
                                <p><?= $exp->description ?></p>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <?php endif; ?>
            <div id="field-studies_short_description" class="col s12">
                <h6>Etudes et certifications</h6>
                <?php the_field('studies_short_description'); ?>
            </div>
            <?php if($studies): ?>
            <div class="col s12">
                <h6><small>Détails</small></h6>
                <div class="list">
                    <?php foreach ($studies ?: [] as $st) :?>
                        <div class="list-item">
                            <div class="img">
                                <img src="<?= $st->image ?: THEME_DEFAULT_IMG; ?>">
                            </div>
                            <div class="info">
                                <h6><?= $st->title ?></h6>
                                <p><?= $st->description ?></p>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <?php endif;?>
            <div id="field-hobbies" class="col s12">
                <h6>Centre d'intérêts</h6>
                <?php the_field('hobbies')?>
            </div>
            <div class="col s12 print-hidden">
                <h6>Filière</h6>
                <div class="col xl3 m6 s12">
                    <?php
                    setup_postdata($post = $training);
                    get_template_part('template-parts/card', 'training');
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <div class="col s12 print-hidden">
                <h6>Autres stagiaires de la filière</h6>
                <?php $loop = tb_get_trainees_by_trainings([$training->ID], [get_the_ID()])?>
                <div class="trainees">
                    <?php while ($loop->have_posts()): $loop->the_post() ?>
                        <?php get_template_part('template-parts/card', 'trainee'); ?>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
        <aside class="col xl3 s12 print-hidden">
            <div>
                <h5>Contacts</h5>
                <div class="socials">
                    <a id="open-email" <?= $emailAttr ?>><i class="fa fa-envelope"></i></a>
                    <a <?= $phoneAttr ?>><i class="fa fa-phone"></i></a>
                    <a <?= $facebookAttr ?>><i class="fa fa-facebook"></i></a>
                    <a <?= $linkedinAttr ?>><i class="fa fa-linkedin"></i></a>
                    <a <?= $foremAttr ?>><span class="forem">FOREM</span></a>
                </div>
                <h5>Projets du stagiaire</h5>
                <?php foreach($projects ?: [] as $post) { setup_postdata($post); ?>
                    <div class="col xl12 m6 s12">
                        <?php get_template_part('template-parts/card', 'project'); ?>
                    </div>
                <?php } wp_reset_postdata(); ?>
            </div>
        </aside>
    </div>
</section>
<script>
    let hasChanged = (field, old) => {
        const item = document.getElementById(field);
        if(old) {
            item.classList.add('tooltip');
            item.setAttribute('data-content', old);
        }
        item.classList.add('changed');
    };
</script>
<?php
if($_GET['preview'] && $post_status === 'pending') {
    $loop = tb_get_trainee_by_user($authorId, true, true);
    $loop->the_post();
    if(get_the_ID()) {
        if($title != get_the_title()) { ?>
            <script>
                hasChanged('field-title', `<?= get_the_title() ?>`);
            </script>
        <?php }
        if($thumbnail != get_post_thumbnail_id()) { ?>
            <script>
                hasChanged('field-thumbnail);
            </script>
        <?php }

        if($questionTraining != get_field('question_training')) { ?>
            <script>
                hasChanged('field-question_training', `<?= get_field('question_training')?>`);
            </script>
        <?php }
        if($questionJob1 != get_field('question_job_1')) { ?>
            <script>
                hasChanged('field-question_job_1', `<?= get_field('question_job_1')?>`);
            </script>
        <?php }
        if($questionJob2 != get_field('question_job_2')) { ?>
            <script>
                hasChanged('field-question_job_2', `<?= get_field('question_job_2')?>`);
            </script>
        <?php }
        if($questionBusiness1 != get_field('question_business_1')) { ?>
            <script>
                hasChanged('field-question_business_1', `<?= get_field('question_business_1')?>`);
            </script>
        <?php }
        if($questionBusiness2 != get_field('question_business_2')) { ?>
            <script>
                hasChanged('field-question_business_2', `<?= get_field('question_business_2')?>`);
            </script>
        <?php }
        if($questionMore != get_field('question_more')) { ?>
            <script>
                hasChanged('field-question_more', `<?= get_field('question_more')?>`);
            </script>
        <?php }

        if($professionalProject != get_field('professional_project')) { ?>
            <script>
                hasChanged('field-professional_project', `<?= get_field('professional_project')?>`);
            </script>
        <?php }

        if($experiencesShortDescription != get_field('experiences_short_description')) { ?>
            <script>
                hasChanged('field-experiences_short_description', `<?= get_field('experiences_short_description')?>`);
            </script>
        <?php }

        if($studiesShortDescription != get_field('studies_short_description')) { ?>
            <script>
                hasChanged('field-studies_short_description', `<?= get_field('studies_short_description')?>`);
            </script>
        <?php }

        if($hobbies != get_field('hobbies')) { ?>
            <script>
                hasChanged('field-hobbies', `<?= get_field('hobbies')?>`);
            </script>
        <?php }
    }
}
?>

<style>
    .changed {
        border: 2px solid red;
    }
</style>


<?php get_template_part('template-parts/search', 'tb_trainees') ?>
<?php get_footer() ?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v10.0&appId=482065732919932&autoLogAppEvents=1" nonce="K5gOBkGm"></script>