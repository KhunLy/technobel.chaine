<?php
$user = wp_get_current_user();
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <title><?php bloginfo('name'); ?><?php wp_title(' - ') ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div>
<header class="tb-theme print-hidden">
    <nav>
        <div class="container" style="padding-bottom: 0">
            <div id="main-menu-button" class="clickable">
                <div class='threebar hamburger'>
                    <div class='bar'></div>
                    <div class='bar'></div>
                    <div class='bar'></div>
                </div>
            </div>
            <div id="main-menu-logo">
                <?php the_custom_logo() ?>
            </div>
            <?php wp_nav_menu([
                'theme_location' => "primary_menu",
                'menu_id' =>  'main-menu',
                'menu_class' =>  'top-menu',
                'container'=> false,
            ]) ?>
            <ul class="top-menu" id="user-menu">
                <?php if (is_user_logged_in()): ?>

                    <li><a href="<?= get_site_url() . '/edit-profile' ?>">Profil</a></li>

                    <li><a href="<?= get_site_url() . '/wp-admin/edit.php?post_type=tb_trainees' ?>">Dashboard</a></li>
                    <li><a href="<?= site_url(); ?>/wp-login.php?action=logout"><i class="fa fa-power-off"></i></a></li>
                <?php else: ?>
                    <li><a href="<?= site_url(); ?>/wp-login.php">Login</a></li>
                <?php endif; ?>
            </ul>
        </div>
        <div id="side-panel" class="collapse">
            <?php wp_nav_menu([
                'theme_location' => "primary_menu",
                'menu_id' =>  'main-menu',
                'menu_class' =>  'side-menu',
                'container'=> false,
            ]) ?>
        </div>
    </nav>
</header>
<main class="tb-theme">
    <div class="container" style="padding-bottom: 0">


