<?php get_header() ?>
<section id="front-last-projects">
<h2>Derniers Projets</h2>
    <div class="row">
        <?php $loop = tb_get_projects(4); ?>
        <?php while ($loop->have_posts()): $loop->the_post(); ?>
            <div class="col xl3 m6 s12">
                <?php get_template_part('template-parts/card', 'project') ?>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <div id="react-last-projects">
    </div>
</section>
<section id="front-highlighted-projects">
    <h2>à la une</h2>
    <div class="row">
        <?php $loop = tb_get_projects(4, 0, true); ?>
        <?php while ($loop->have_posts()): $loop->the_post(); ?>
            <div class="col xl3 m6 s12">
                <?php if(get_post_type(get_the_ID()) === 'post') {
                    get_template_part('template-parts/card', 'project');
                } else {
                    get_template_part('template-parts/card', 'info');
                } ?>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <div id="react-sticky-projects">
    </div>
</section>
<section id="front-trainings">
    <h2>Nos filères</h2>
    <div class="row">
        <?php $loop = tb_get_trainings(4); ?>
        <?php while ($loop->have_posts()): $loop->the_post(); ?>
            <div class="col xl3 m6 s12">
                <?php get_template_part('template-parts/card', 'training') ?>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <div id="react-last-trainings">
    </div>
</section>
<?php get_template_part('template-parts/search', 'post') ?>
<?php get_footer() ?>
