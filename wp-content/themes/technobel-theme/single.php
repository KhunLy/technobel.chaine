<?php
$categories = get_the_category();
$terms = get_the_terms(get_the_ID(), 'tb_skills');
$trainings = array_filter(get_field('trainings') ?: [], function($item){
    return $item->post_status === 'publish';
});
$students = array_filter(get_field('projects') ?: [], function($item){
    return $item->post_status === 'publish';
});
?>

<?php get_header() ?>
<section>
    <div class="row">
        <div class="col xl9 s12">
            <h3><?php the_title() ?></h3>
            <div class="projects-img-container col s12">
                <div class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="80">
                    <img src="<?php the_post_thumbnail_url(); ?>">
                    <?php
                    $images = get_field('gallery');
                    if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <img src="<?php echo $image['full_image_url']; ?>">
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="socials-share">
                    <a target="_blank" href="<?= 'https://www.facebook.com/dialog/share?app_id=482065732919932&display=popup&href=' . get_the_permalink()?>">
                        <button class="fb-button"><i class="fa fa-facebook"></i> Partager</button>
                    </a>
                    <a target="_blank" href="<?= 'http://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink() ?>">
                        <button class="linkedin-button"><i class="fa fa-linkedin"></i> Partager</button>
                    </a>
                </div>
            </div>
            <div class="col s12">
                <h6>Contexte</h6>
                <?php the_content(); ?>
                <h6>Challenge</h6>
                <?php the_field('challenge'); ?>
                <h6>Résultats</h6>
                <?php the_field('results'); ?>
            </div>
            <div class="col s12 d-flex">
                <div class="tags">

                </div>
                <div class="tags">
                    <?php if($terms): ?>
                        <?php foreach($terms as $term): ?>
                            <?php $color = get_field('color', $term) ?>
                            <div class="tag skill" style="border-color: <?= $color ?>; color: <?= $color ?>">
                                <?= $term->name ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if($trainings) :?>
            <div class="col s12">
                <h5>Filères ayant participé au projet</h5>
                <div class="row">
                    <?php foreach($trainings as $post) { setup_postdata($post); ?>
                        <div class="col xl3 m6 s12">
                            <?php get_template_part('template-parts/card', 'training'); ?>
                        </div>
                    <?php } wp_reset_postdata(); ?>
                </div>
            </div>
            <?php endif;?>
            <?php if($students) :?>
            <div class="col s12">
                <h5>Étudiants ayant participé au projet</h5>
                <div class="row">
                    <?php foreach($students as $post) { setup_postdata($post); ?>
                        <div class="trainees">
                            <?php get_template_part('template-parts/card', 'trainee'); ?>
                        </div>
                    <?php } wp_reset_postdata(); ?>
                </div>
            </div>
            <?php endif;?>
        </div>
        <?php $loop = tb_get_projects_by_categories($categories, [get_the_ID()], 4);?>
        <?php if($loop->have_posts()): ?>
        <aside class="col xl3 s12">
            <div>
                <h5>Projets similaires</h5>
                <?php while ($loop->have_posts()): $loop->the_post(); ?>
                    <div class="col xl12 m6 s12">
                        <?php get_template_part('template-parts/card', 'project') ?>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        </aside>
        <?php endif; ?>
    </div>
</section>

<?php get_template_part('template-parts/search', 'post') ?>
<?php get_footer() ?>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v10.0&appId=482065732919932&autoLogAppEvents=1" nonce="K5gOBkGm"></script>
