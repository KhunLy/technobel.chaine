<?php get_header() ?>
<?php get_template_part('template-parts/search', 'post') ?>
<div class="row">
    <div class="search-results">
        <?php while (have_posts()): the_post(); ?>
            <?php get_template_part('template-parts/card') ?>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <?php the_posts_pagination([
        'prev_text' => 'Précédent',
        'next_text' => 'Suivant',
    ]); ?>
</div>
<?php get_footer() ?>