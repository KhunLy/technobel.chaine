<?php
/*** constantes php ***/
define("THEME_URI", get_stylesheet_directory_uri());
define("THEME_DEFAULT_IMG", get_stylesheet_directory_uri() . '/assets/images/default.png');
define("THEME_ASSETS_URI", get_stylesheet_directory_uri() . '/assets');
define("THEME_ASSETS_CSS_URI", get_stylesheet_directory_uri() . '/assets/css');
define("THEME_ASSETS_JS_URI", get_stylesheet_directory_uri() . '/assets/js');
define("THEME_ASSETS_IMAGE_URI", get_stylesheet_directory_uri() . '/assets/images');
define("THEME_NODE_MODULES_URI", get_stylesheet_directory_uri() . '/node_modules');
define("THEME_API_URI", get_rest_url());
/*** end constantes php ***/

/*** constantes js ***/
define('THEME_JS_DATAS', [
    'API_URI' => get_rest_url(),
    'ASSETS_URI' => THEME_ASSETS_URI,
    'DEFAULT_IMG' => THEME_ASSETS_IMAGE_URI . '/default.png',
    'SITE_URL' => get_bloginfo('url'),
    // need for rest api authentication
    'NONCE' => wp_create_nonce( 'wp_rest' )
]);
/*** end constantes js ***/

/*** includes ***/
include_once 'inc/tb_init.php';
include_once 'inc/tb_security.php';
include_once 'inc/tb_filter.class.php';
include_once 'inc/tb_query_filter.php';
include_once 'inc/tb_repository.php';
include_once 'inc/tb_register_scripts.php';
/*** end includes ***/

/*** hooks ***/
add_action('after_setup_theme', 'tb_init_theme');
add_action('admin_menu', 'tb_init_admin_menu');
add_action('wp_enqueue_scripts', 'tb_register_scripts');
add_action('login_enqueue_scripts', 'tb_login_stylesheet' );
add_action('admin_enqueue_scripts', 'tb_admin_theme_style');
add_action('pre_get_posts', 'tb_filter_posts');
/*** end hooks ***/

/*** filters ***/
add_filter( 'posts_where', 'tb_posts_where', 10, 2 );
add_filter( 'login_redirect', 'tb_login_redirect', 10, 3 );
add_filter( 'excerpt_length', function($length) { return 20; });
add_filter( 'get_custom_logo', 'tb_change_logo_link' );
function tb_change_logo_link() {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url">%2$s</a>',
        esc_url( 'https://www.technobel.be' ),
        wp_get_attachment_image( $custom_logo_id, 'full', false, array(
            'class'    => 'custom-logo',
        ) )
    );
    return $html;
}
/*** end filters ***/

function tb_filter_by_the_author() {
	$params = array(
		'name' => 'author', // this is the "name" attribute for filter <select>
		'show_option_all' => 'All authors' // label for all authors (display posts without filter)
	);
 
	if ( isset($_GET['user']) )
		$params['selected'] = $_GET['user']; // choose selected user by $_GET variable
 
	wp_dropdown_users( $params ); // print the ready author list
}
 
add_action('restrict_manage_posts', 'tb_filter_by_the_author');



