<?php get_header() ?>
<section>
    <div class="row">
        <div class="col s12">
            <h3><?php the_title() ?></h3>
            <div class="projects-img-container col s12">
                <div class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="80">
                    <img src="<?php the_post_thumbnail_url(); ?>">
                </div>
            </div>
            <div class="col s12">
                <h6>Contexte</h6>
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>