
<a href="<?php the_permalink(); ?>" class="blk">
    <div class="tb-card">
        <div class="ribbon ribbon-top-left"><span>News</span></div>
        <div class="card-content">
            <div class="card-top">
                <img src="<?= get_the_post_thumbnail_url(null,'post-thumbnail') ?: THEME_DEFAULT_IMG; ?>" alt="Card image cap">
            </div>
            <div class="card-center">
                <h5><?php the_title(); ?></h5>
                <p><?php the_excerpt(); ?></p>
            </div>
        </div>
    </div>
</a>