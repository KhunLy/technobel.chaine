
<h1>Contacts et partages</h1>
<div class="row">
    <div class="col">
        <div id="chart1" class="card">

        </div>
    </div>
    <div class="col">
        <div id="chart2" class="card">

        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div id="chart3" class="card">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <table id="table" class="table card">
            <thead>
            <tr>
                <th>Mois</th>
                <th>Type</th>
                <th>Filière</th>
                <th>Quantité</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<h1>Pages</h1>

<?php
$countTrainings = tb_get_trainings()->post_count;

$countTrainees = tb_get_trainees()->post_count;

$countProjects = tb_get_projects(-1,0,true)->post_count + tb_get_projects()->post_count;

?>

<div class="row">
    <div class="col">
        <div class="card">
            <h4>Nombre de Filières publiées: <?= $countTrainings ?></h4>
            <h4>Nombre de Stagiaires publiés: <?= $countTrainees ?></h4>
            <h4>Nombre de Projets publiés: <?= $countProjects ?></h4>
        </div>
    </div>

</div>
