<?php
$categories = get_categories();
$skills = get_terms([
    'taxonomy' => 'tb_skills'
]);
$filter = new tbfilter($_GET);
?>
<section id="search" class="print-hidden">
    <form action="<?= get_site_url();?>/archives/stagiaires" method="get">
        <h4 class="blanc">Rechercher un étudiant</h4>
        <div class="row">
            <div class="col xl6 m12 s12">
                <h6 class="blanc">Nom ou Prénom</h6>
                <div class="col s12 input-field tb">
                    <input id="name" value="<?= $filter->getS() ?>" name="s" type="text">
                    <label for="name" >Nom ou prénom</label>
                </div>
                <h6 class="blanc">Email</h6>
                <div class="col s12 input-field tb">
                    <input id="email" value="<?= $filter->getEmail() ?>" name="email" type="text">
                    <label for="email" >Email</label>
                </div>
            </div>
            <div class="col xl6 m12 s12">
                <h6 class="blanc">Technologies</h6>
                <div class="col s12 tb input-field">
                    <select name="skills[]" multiple>
                        <?php foreach ($skills as $skill): ?>
                            <option <?= in_array($skill->term_id, $filter->getSkills()) ? 'selected' : '' ?>
                                value="<?= $skill->term_id ?>"><?= $skill->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 button-container">
                <button class="tb search">Rechercher <i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</section>