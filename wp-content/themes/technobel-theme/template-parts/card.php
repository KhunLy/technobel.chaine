
<a href="<?php the_permalink(); ?>" class="search-card blk">
    <div class="search-card-content">
        <div class="search-card-content-img">
            <?php if(get_field('color')): ?>
                <div style="background-color: <?php the_field('color') ?>; height: 100%"></div>
            <?php else: ?>
                <img src="<?= get_the_post_thumbnail_url(null,'post-thumbnail') ?: THEME_DEFAULT_IMG; ?>">
            <?php endif ?>
        </div>
        <div class="search-card-content-info">
            <h6><?php the_title() ?></h6>
        </div>
    </div>
</a>