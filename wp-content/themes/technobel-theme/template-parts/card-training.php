
<div class="tb-card card-training">
<a class="blk" href="<?php the_permalink(); ?>">
    <div class="card-content">
        <div class="card-top training-top" style="background-color: <?php the_field('color') ?>">
            <!-- <img src="<?= get_the_post_thumbnail_url(null, 'post-thumbnail') ?: THEME_DEFAULT_IMG; ?>" alt="Card image cap"> -->
            <h5><?php the_title(); ?></h5>
        </div>
        <!-- <div class="card-center">
            <p><?php the_excerpt(); ?></p>
        </div>
        <div class="card-bottom right">
            <a class="blk" href="<?php the_permalink(); ?>">
                <button class="inverse tb">Plus d'info</button>
            </a>
        </div> -->
    </div>
    </a>
</div>
