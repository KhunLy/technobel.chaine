<?php
$categories = get_categories();
$skills = get_terms([
    'taxonomy' => 'tb_skills'
]);
$filter = new tbfilter($_GET);
?>
<section id="search">
    <form action="<?= get_site_url();?>/projets" method="get">
        <h4 class="blanc">Recherche de projets</h4>
        <div class="row">
            <div class="col xl6 m12 s12">
                <h6 class="blanc">Période</h6>
                <div class="col m6 s12 input-field tb">
                    <label for="startDate" class="blanc">Date de début</label>
                    <input id="startDate" value="<?= $filter->getStartDate() ?>" name="start_date" type="text" class="datepicker">
                </div>
                <div class="col m6 s12 input-field tb">
                    <label for="endDate">Date de fin</label>
                    <input id="endDate" value="<?= $filter->getEndDate() ?>" name="end_date" type="text" class="datepicker">
                </div>
            </div>
            <div class="col xl6 m6 s12">
                <h6 class="blanc">Technologies</h6>
                <div class="col s12 tb input-field">
                    <select name="skills[]" multiple class="bleufonce">
                        <?php foreach ($skills as $skill): ?>
                            <option <?= in_array($skill->term_id, $filter->getSkills()) ? 'selected' : '' ?>
                                    value="<?= $skill->term_id ?>"><?= $skill->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 button-container">
                <button class="tb search">Rechercher <i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</section>