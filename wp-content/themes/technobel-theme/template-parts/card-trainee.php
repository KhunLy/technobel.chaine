<a href="<?php the_permalink(); ?>" class="trainee">
    <div class="img-container">
        <img src="<?= get_the_post_thumbnail_url(null, 'post-thumbnail') ?: THEME_DEFAULT_IMG; ?>">
    </div>
    <p><?php the_title() ?></p>
</a>
