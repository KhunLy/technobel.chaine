<?php
$args = [
    'post_type' => 'tb_folder',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'order_by' => 'post_title',
    'order' => 'asc'
];

$loop = new WP_Query($args);
?>

<div class="container">
        <div class="row">
    <?php while ($loop->have_posts()): $loop->the_post() ?>
            <div class="col">
                <div class="folder">
                    <div class="folder-content open-modal" data-target="modal-<?= get_the_ID() ?>"
                        <?= get_the_post_thumbnail_url() ? 'style="background-image: url(\''. get_the_post_thumbnail_url(null, 'thumbnail') .'\')"' : ''?>
                    >
                        <?php if(!get_the_post_thumbnail_url()):?>
                        <?php the_title() ?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        <div id="modal-<?= get_the_ID() ?>" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2><?php the_title() ?></h2>
                </div>
                <div class="modal-body">
                    <table>
                        <thead>
                        <tr>
                            <td>Nom</td>
                            <td>Nom du fichier</td>
                            <td>Télécharger</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i=0; $i<50; $i++) : ?>
                            <?php
                            $file = get_field('file_' . $i);
                            if( $file ): ?>
                            <tr>
                                <td><?= $file['title']; ?></td>
                                <td><?= $file['filename']; ?></td>
                                <td><a download href="<?= $file['url']; ?>">télécharger</a></td>
                            </tr>
                            <?php endif; ?>
                        <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    <?php endwhile; ?>
        </div>
</div>


<style>
    .modal a {
        display: inline-block;
        text-decoration: none;
    }

    .modal a > .file-container {
        width: 75px; display: flex; flex-direction: column; align-items: center; justify-content: center
    }

    .modal a > .file-container > .file {
        height: 75px;width: auto
    }

    .modal a > .file-container > .file-text {
        font-size: 10px;word-break: break-word;text-align: center
    }

    table {
        width: 100% !important;
    }

    .dataTables_info {
        display: none;
    }
</style>