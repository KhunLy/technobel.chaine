<div class="padding">
    <input id="input-file" type="file">
    <input id="input-submit" class="button" type="submit">
    <div class="wrapper">
        <div class="progress-bar">
            <span class="progress-bar-fill" style="width: 0;"></span>
        </div>
    </div>
    <div id="result">

    </div>
</div>

<style>
    .wrapper {
        margin-top: 20px;
        width: 50%;
    }

    @media screen and (max-width: 991px) {
        .wrapper {
            width: 100%;
        }
    }

    .progress-bar {
        width: 100%;
        background-color: #e0e0e0;
        padding: 3px;
        border-radius: 3px;
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, .2);
    }

    .progress-bar-fill {
        display: block;
        height: 22px;
        background-color: #659cef;
        border-radius: 3px;
        transition: width 500ms ease-in-out;
    }
</style>
<script>
    jQuery($ => {
        const inputFileElement = document.getElementById('input-file');
        const $inputSubmitElement = $('#input-submit');
        const $resultElement = $('#result');
        const $progressBarElement = $('.progress-bar-fill');

        $inputSubmitElement.on('click', e => {
            if(inputFileElement.files) {
                $resultElement.empty();
                enableButton(false);
                $progressBarElement.css({ width: 0 });
                const reader = new FileReader();
                reader.readAsText(inputFileElement.files[0]);
                reader.onload = e => {
                    let lines = processData(e.target.result);
                    let finished = 0;
                    let total = lines.length;
                    for(let line of lines) {
                        const fd = new FormData();
                        for (let key of Object.keys(line)) {
                            fd.append(key, line[key]);
                        }
                        $.ajax({
                            url: '<?= THEME_API_URI ?>api/users',
                            data: fd,
                            type: 'POST',
                            contentType: false,
                            processData: false,
                        }).done(result => {
                            if(result !== true) {
                                $resultElement.append(`<p>Erreur à la ligne ${lines.indexOf(line) + 1} : ${result}</p>`)
                            }
                        }).fail((xhr, status, ) => {
                            //messageElement$.html(`<p>Process Error ${status}</p>`);
                        }).always(() => {
                            finished++;
                            $progressBarElement.css({ width: `${finished*100/total}%` });
                            if(finished === total) {
                                enableButton(true);
                            }
                        });
                    }

                }
            }
        });

        function enableButton(flag) {
            $inputSubmitElement.attr('disabled', !flag);
        }

        function processData(allText) {
            const allTextLines = allText.split(/\r\n|\n/);
            const lines = [];
            const headingLine = allTextLines[0];
            allTextLines.splice(0,1);
            const headings = headingLine.split(';');

            for (let entry of allTextLines) {
                const array = entry.split(';');
                const data = {};
                for(let i=0;i < array.filter(x => x.length >= 4).length; i++) {
                    data[headings[i].toLowerCase()] = array[i];
                }
                lines.push(data);
            }
            return lines;
        }
    });
</script>