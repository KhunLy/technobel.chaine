
<a href="<?php the_permalink(); ?>" class="blk">
    <div class="tb-card">
        <div class="card-content">
            <div class="card-top">
                <img src="<?= get_the_post_thumbnail_url(null, 'post-thumbnail') ?: THEME_DEFAULT_IMG; ?>" alt="Card image cap">
            </div>
            <div class="card-center">
                <h5><?php the_title(); ?></h5>
                <p><?php the_excerpt(); ?></p>
            </div>
            <div class="card-bottom">
                <div class="tags">

                </div>
                <div class="tags">
                    <?php $terms = get_the_terms(get_the_ID(), 'tb_skills') ?>
                    <?php if($terms): ?>
                        <?php foreach(array_splice($terms, 0 ,2) as $term): ?>
                            <?php $color = get_field('color', $term) ?>
                            <div class="tag skill" style="border-color: <?= $color ?>; color: <?= $color ?>">
                                <?= $term->name ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</a>