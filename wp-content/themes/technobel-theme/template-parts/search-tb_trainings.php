<?php
$categories = get_categories();
$skills = get_terms([
    'taxonomy' => 'tb_skills'
]);
$filter = new tbfilter($_GET);
?>
<section id="search">
    <form action="<?= get_site_url();?>/archives/filieres" method="get">
        <h4 class="blanc">Recherche de filières</h4>
        <div class="row">
            <div class="col xl6 m12 s12">
                <h6 class="blanc">Année</h6>
                <div class="col s12 input-field tb">
                    <select name="post_year" type="text">
                        <option></option>
                        <?php for ($i=date("Y"); $i>1999;$i--): ?>
                        <option <?= $i == $filter->getYear() ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                    </select>
                </div>
                <h6 class="blanc">Titre</h6>
                <div class="col s12 input-field tb">
                    <input id="title" value="<?= $filter->getTitle() ?>" name="title" type="text">
                    <label for="title" >Titre</label>
                </div>
            </div>
            <div class="col xl6 m12 s12">
                <h6 class="blanc">Technologies</h6>
                <div class="col s12 tb input-field">
                    <select name="skills[]" multiple>
                        <?php foreach ($skills as $skill): ?>
                            <option <?= in_array($skill->term_id, $filter->getSkills()) ? 'selected' : '' ?>
                                value="<?= $skill->term_id ?>"><?= $skill->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 button-container">
                <button class="tb search">Rechercher <i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</section>