<?php

function tb_get_projects($limit = -1, $offset = 0, $is_sticky = false) {
    $queryArgs = [
        'post_type' => ['post', 'tb_infos'],
        'post_status' => 'publish',
        'posts_per_page' => $limit,
        'offset' => $offset,
        'order_by' => 'publish_date',
        'order' => 'desc'
    ];
    if(!$is_sticky) {
        $queryArgs['post__not_in'] = get_option( 'sticky_posts' );
    }
    else {
        $queryArgs['post__in'] = get_option( 'sticky_posts' );
    }
    return new WP_Query($queryArgs);
}

function tb_get_projects_by_categories($categories, $ignoreIds = [], $limit = -1, $offset = 0) {
    $queryArgs = [
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => $limit,
        'offset' => $offset,
        'ignore_sticky_posts' => 1,
        'post__not_in' => $ignoreIds,
        'order_by' => 'publish_date',
        'order' => 'desc'
    ];
    $tax_query = [
        'relation' => 'AND',
    ];
    $tax_query[] = [
        'taxonomy' => 'category',
        'field'    => 'term_id',
        'terms'    => array_map(function($cat){ return $cat->term_id; }, $categories),
        'operator' => 'IN',
    ];
    $queryArgs['tax_query'] = $tax_query;
    return new WP_Query($queryArgs);
}

function tb_get_trainings($limit = -1, $offset = 0) {
    $queryArgs = [
        'post_type' => 'tb_trainings',
        'post_status' => 'publish',
        'posts_per_page' => $limit,
        'offset' => $offset,
        'order_by' => 'publish_date',
        'order' => 'desc'
    ];
    return new WP_Query($queryArgs);
}

function tb_get_trainees($limit = -1, $offset = 0, $is_sticky = false) {
    $queryArgs = [
        'post_type' => 'tb_trainees',
        'post_status' => 'publish',
        'posts_per_page' => $limit,
        'offset' => $offset,
        'order_by' => 'publish_date',
        'order' => 'desc'
    ];
    return new WP_Query($queryArgs);
}

function tb_get_trainees_by_trainings($trainings, $ignoreIds = [], $limit = -1, $offset = 0) {
    $queryArgs = [
        'post_type' => 'tb_trainees',
        'post_status' => 'publish',
        'posts_per_page' => $limit,
        'offset' => $offset,
        'post__not_in' => $ignoreIds,
        'order_by' => 'title',
        'order' => 'asc',
        'meta_query' => [
            'relation' => 'AND', [
                'key'     => 'training',
                'value'   => $trainings,
                'compare' => 'IN',
            ]
        ]
    ];
    return new WP_Query($queryArgs);
}
function tb_get_trainee_by_user($userId, $published, $isStudent) {
    $queryArgs = [
        'post_type' => 'tb_trainees',
        'post_status' => $published ? ['publish'] : ['publish', 'pending', 'draft', 'auto-draft', 'private', 'inherit'],
        'posts_per_page' => 1,
        'offset' => 0,
        'order_by' => 'date',
        'order' => 'desc'
    ];
    if($isStudent) {
        $queryArgs['author__in'] = [$userId];
    }
    else {
        // id of John Lennon
        $queryArgs['author__in'] = [480];
    }
    return new WP_Query($queryArgs);
}

function tb_get_skills() {
    return array_map(function($term){
        return [ 'term' => $term, 'color' => get_field('color', $term) ];
    }, get_terms('tb_skills', ['hide_empty' => false]) ?: []);
}

function tb_get_soft_skills() {
    return array_map(function($term){
        return [ 'term' => $term, 'color' => get_field('color', $term) ];
    }, get_terms('tb_soft_skills', ['hide_empty' => false]) ?: []);
}