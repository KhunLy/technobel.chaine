<?php
function tb_init_theme() {
    if (!is_admin()) {
        show_admin_bar(false);
    }

    add_theme_support('post-thumbnails');
    add_theme_support('custom-logo');
}

function tb_init_admin_menu() {
    add_menu_page('kpi',
        'Infos de la plateforme', 'edit_dashboard',
        'kpi', function() {
            get_template_part('template-parts/admin', 'kpi');
        });

    add_submenu_page('users.php', 'users_import',
        'Importer des utilisateurs', 'create_users',
        'user_import', function() {
            get_template_part('template-parts/admin', 'users_import');
        });

    add_menu_page('tools','Outils', 'edit_posts',
        'tools', function() {
            get_template_part('template-parts/admin', 'tools');
        }, '', 20);
}