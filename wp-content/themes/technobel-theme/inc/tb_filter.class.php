<?php
class tbfilter {

    private $title;
    private $startDate;
    private $endDate;
    private $categories;
    private $skills;
    private $s;
    private $email;
    private $year;

    public function __construct($get)
    {
        $this->startDate = isset($get['start_date']) && trim($get['start_date']) != '' ? $get['start_date'] : null;
        $this->endDate = isset($get['end_date']) && trim($get['end_date']) != '' ? $get['end_date'] : null;
        $this->year = $get['post_year'] ?: null;
        $this->title = $get['title'] ?: null;
        $this->s = $get['s'] ?: null;
        $this->email = $get['email'] ?: null;
        $this->categories = $get['categories'] ?: [];
        $this->skills = $get['skills'] ?: [];
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return tbfilter
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getS()
    {
        return $this->s;
    }

    /**
     * @param mixed|null $s
     * @return tbfilter
     */
    public function setS($s)
    {
        $this->s = $s;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed|null $email
     * @return tbfilter
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     * @return tbfilter
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     * @return tbfilter
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     * @return tbfilter
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     * @return tbfilter
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed|null $year
     * @return tbfilter
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

}