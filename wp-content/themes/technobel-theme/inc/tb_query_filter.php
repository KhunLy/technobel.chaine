<?php

function tb_filter_posts($query) {
    if(!$query->is_main_query()) {
        return;
    }

    $filter = new tbfilter($_GET);

    $query->set('ignore_sticky_posts', 1);

    $tax_query = [
        'relation' => 'AND',
    ];

    if(!empty($filter->getCategories())) {
        $tax_query[] = [
            'taxonomy' => 'category',
            'field'    => 'term_id',
            'terms'    => $filter->getCategories(),
            'operator' => 'IN',
        ];
    }

    if(!empty($filter->getSkills())) {
        $tax_query[] = [
            'taxonomy' => 'tb_skills',
            'field'    => 'term_id',
            'terms'    => $filter->getSkills(),
            'operator' => 'IN',
        ];
    }
    $query->set('tax_query', $tax_query);

    if($filter->getTitle()) {
        $query->set('starts_with', $filter->getTitle());
    }


    if($filter->getStartDate() != null || $filter->getEndDate() != null) {
        $date_query = [
            'inclusive' => true,
        ];

        if($filter->getStartDate() != null) {
            $date = explode('/', $filter->getStartDate());
            if(count($date) >= 3) {
                $date_query['after'] = [
                    'year' => $date[2],
                    'month' => $date[1],
                    'day' => $date[0],
                ];
            }
        }

        if($filter->getEndDate() != null) {
            $date = explode('/', $filter->getEndDate());
            if(count($date) >= 3) {
                $date_query['before'] = [
                    'year' => $date[2],
                    'month' => $date[1],
                    'day' => $date[0],
                ];
            }
        }
        $query->set('date_query', $date_query);
    }

    if($filter->getEmail() != null) {
        $meta_query = [
            'relation' => 'AND',
        ];
        $meta_query[] = [
            'key' => 'email',
            'value' => '^'.$filter->getEmail(),
            'compare' => 'REGEXP',
        ];
        $query->set('meta_query', $meta_query);
    }

    if($filter->getYear()) {
        $meta_query = [
            'relation' => 'AND',
        ];
        $meta_query[] = [
            'key' => 'year',
            'value' => $filter->getYear()
        ];
        $query->set('meta_query', $meta_query);
    }

}

function tb_posts_where($where,$query ) {
    global $wpdb;
    $starts_with = esc_sql($query->get( 'starts_with' ));
    if ($starts_with) {
        $where .= " AND $wpdb->posts.post_title LIKE '$starts_with%'";
    }
    return $where;
}