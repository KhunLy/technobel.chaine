<?php
define('VERSION', '1.1');
function tb_register_scripts() {
    /*** axios ***/
    wp_enqueue_script('theme_axios', THEME_NODE_MODULES_URI . '/axios/dist/axios.min.js');
    /*** end axios ***/

    /*** react ***/
    wp_enqueue_script('theme_react', THEME_NODE_MODULES_URI . '/react/umd/react.production.min.js', [], null, true);
    wp_enqueue_script('theme_react_dom', THEME_NODE_MODULES_URI . '/react-dom/umd/react-dom.production.min.js', ['theme_react'], null, true);
    /*** end react ***/

    /*** jQuery ***/
    wp_enqueue_script('theme_jquery', THEME_NODE_MODULES_URI . '/jquery/dist/jquery.min.js');
    /*** end JQuery ***/

    /*** popper ***/
    // temporary added in footer.php
    //wp_enqueue_script('theme_popper', THEME_NODE_MODULES_URI . '/popper.js/dist/esm/popper.min.js');
    /*** end popper ***/

    /*** toastr ***/
    wp_enqueue_style('theme_toastr_css', THEME_NODE_MODULES_URI . '/toastr/build/toastr.min.css');
    wp_enqueue_script('theme_toastr_js', THEME_NODE_MODULES_URI . '/toastr/build/toastr.min.js', [], null, true);
    /*** end toastr ***/

    /*** tippy ***/
    wp_enqueue_style('theme_tippy_css', THEME_NODE_MODULES_URI . '/tippy.js/dist/tippy.css');
    wp_enqueue_style('theme_tippy_scale_css', THEME_NODE_MODULES_URI . '/tippy.js/animations/scale-extreme.css');
    wp_enqueue_script('theme_tippy_js', THEME_NODE_MODULES_URI . '/tippy.js/dist/tippy.umd.min.js', [], null, true);
    /*** end tippy ***/

    /*** fontawesome ***/
    wp_enqueue_style('theme_fontawesome_all', THEME_NODE_MODULES_URI . '/@fortawesome/fontawesome-free/css/all.min.css');
    wp_enqueue_style('theme_fontawesome_brands', THEME_NODE_MODULES_URI . '/@fortawesome/fontawesome-free/css/brands.min.css');
    wp_enqueue_style('theme_fontawesome_v4-shims', THEME_NODE_MODULES_URI . '/@fortawesome/fontawesome-free/css/v4-shims.min.css');
    /*** end fontawesome ***/

    /*** materialize ***/
    wp_enqueue_style('theme_materialize_css', THEME_NODE_MODULES_URI . '/materialize-css/dist/css/materialize.min.css');
    wp_enqueue_script('theme_materialize_js', THEME_NODE_MODULES_URI . '/materialize-css/dist/js/materialize.min.js');
    /*** end materialize ***/

    /*** main ***/
    wp_enqueue_style('theme_main_css', THEME_URI . '/style.css', ['theme_materialize_css', 'theme_toastr_css']);
    wp_enqueue_script('theme_main_js', THEME_ASSETS_JS_URI . '/main.js', ['theme_materialize_js', 'theme_tippy_js', 'theme_toastr_js'], null, true);
    /*** end main ***/

    /*** pages ***/
    /*** front page ***/
    if (is_front_page()) {
        wp_enqueue_script('theme_front_jsx', THEME_ASSETS_JS_URI . '/front-page.js', ['theme_main_js', 'theme_axios'], null, true);
        wp_enqueue_style('theme_front_css', THEME_ASSETS_CSS_URI . '/front-page.css', ['theme_main_css']);
    }

    else if(is_single()) {

        /*** fotorama ***/
        wp_enqueue_style('theme_fotorama_css', 'https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css');
        wp_enqueue_script('theme_fotorama_js', 'https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js', [], null, true);
        /*** end fotorama ***/

        wp_enqueue_style('theme_print_css', THEME_ASSETS_CSS_URI . '/print.css', ['theme_main_css', 'theme_single_css'], false, 'print');

        wp_enqueue_style('theme_single_css', THEME_ASSETS_CSS_URI . '/single.css', ['theme_main_css']);
        wp_enqueue_script('theme_single_js', THEME_ASSETS_JS_URI . '/single.js', ['theme_main_js', 'theme_axios'], null, true);
    }
    if(is_page('edit-profile')) {
        wp_enqueue_media();
        wp_enqueue_style('theme_single_css', THEME_ASSETS_CSS_URI . '/single.css', ['theme_main_css']);
        wp_enqueue_script('theme_file_dialog', THEME_ASSETS_JS_URI . '/lib/file_dialog.js');
        wp_enqueue_script('theme_profile_js', THEME_ASSETS_JS_URI . '/edit-profile.js', ['theme_main_js', 'theme_axios', 'theme_file_dialog', 'theme_react', 'theme_react_dom'], null, true);
    }
    /*** end front page ***/
    /*** end pages ***/

    /*** constants ***/
    // added to main js - add theme_main_js dependencies if you need constants in js files
    wp_localize_script('theme_main_js', 'THEME_CONST', THEME_JS_DATAS);
    /*** end constants ***/
}

function tb_login_stylesheet() {
    wp_enqueue_style( 'theme_login_css', THEME_ASSETS_CSS_URI . '/login.css' );
    wp_enqueue_script( 'theme_login_js', THEME_ASSETS_JS_URI . '/login.js', [], null, true );
}

function tb_admin_theme_style($hook) {
    wp_enqueue_style('theme_admin_css', THEME_ASSETS_CSS_URI . '/admin.css');
    if($hook == 'toplevel_page_kpi') {
        wp_enqueue_script('theme_admin_axios', THEME_NODE_MODULES_URI . '/axios/dist/axios.min.js', [], null, true);
        wp_enqueue_script('theme_admin_highcharts', 'https://code.highcharts.com/highcharts.js', [], null, true);
        wp_enqueue_script('theme_admin_datatable', 'https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js', [], null, true);
        wp_enqueue_script('theme_admin_datatable_buttons', 'https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js', [], null, true);
        wp_enqueue_script('theme_admin_datatable_gzip', 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', [], null, true);
        wp_enqueue_script('theme_admin_datatable_pdf', 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js', [], null, true);
        wp_enqueue_script('theme_admin_datatable_pdf_fonts', 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js', [], null, true);
        wp_enqueue_script('theme_admin_datatable_html_buttons', 'https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js', [], null, true);
        wp_enqueue_script('theme_admin_kpi', THEME_ASSETS_JS_URI . '/admin-kpi.js', ['theme_admin_axios', 'theme_admin_highcharts'], null, true);
        wp_enqueue_style('theme_admin_datatable_css', 'https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css');
        wp_enqueue_style('theme_admin_datatable_buttons_css', 'https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css');
        wp_enqueue_style('theme_admin_kpi', THEME_ASSETS_CSS_URI . '/admin-kpi.css');
        wp_localize_script('theme_admin_kpi', 'THEME_CONST', THEME_JS_DATAS);
    }
    if($hook == 'toplevel_page_tools') {
        wp_enqueue_script('theme_admin_datatable', 'https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js', [], null, true);
        wp_enqueue_style('theme_admin_datatable_css', 'https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css');
        wp_enqueue_script('theme_admin_tools', THEME_ASSETS_JS_URI . '/admin-tools.js', [], null, true);
    }
}


