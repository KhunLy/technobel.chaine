<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'GzZ xa<w<+My|-GY7Y-#4Nwr1^apiijrTI*kzqLq5uQD%Ul[1EX_C>RA/;( aK81' );
define( 'SECURE_AUTH_KEY',  'd%DPAurvQndG`YxM?io`4UJTf6c%gP1tyY;Q5~P}.25-QzU%A}ay/n6Q`0>fr^C~' );
define( 'LOGGED_IN_KEY',    'F;0;!2M$wHT 9EIZ{*-jGC#QZvPlp4kN>CXRFg@A]SGn9e?wnlwEF8E*gF+78clW' );
define( 'NONCE_KEY',        'S`hFTvW_3hSIybQ,CI(&S^,[p}o?};o_!?th5g*x)[#C6)k26a,RzDQRpdVKxKpo' );
define( 'AUTH_SALT',        '$Z>y<D@O$|I^}]Grq6B.+PJ:i~eE@u)gL?YFXS!tS}#vo|v^Y2veqm~[jf;I JDu' );
define( 'SECURE_AUTH_SALT', 'zI>0W29=HQyv7Szcc)/e4FA<+Boer$VBN1>hAuM`X|=BXs7AF@mUiWYK;OMJ%7|w' );
define( 'LOGGED_IN_SALT',   'wro)F!YQ!Me6iIA*G^*3O<5VFeWKI-WhFe6 b;{$RDf%nAtL<zVE^+cZ&]!^Bk1k' );
define( 'NONCE_SALT',       'g/,)B>T^w<@j2I,>1u`OHUOVx4]y;+3{;Gv3{.4,bB{9Mnl g^JWwlA?a KiQNxM' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
